<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------
namespace upload\driver;

use think\facade\Filesystem;
use upload\FileBase;
use upload\trigger\SaveDb;

/**
 * 本地上传
 * Class Local
 * @package EasyAdmin\upload\driver
 */
class Local extends FileBase
{
    /**
     * 重写上传方法
     * @return array|void
     */
    public function save()
    {
        parent::save();
        $file_info = [
            'upload_type' => $this->uploadType,
            'original_name' => $this->file->getOriginalName(),
            'member_id' => $this->member_id,
            'mime_type' => $this->file->getOriginalMime(),
            'file_ext' => strtolower($this->file->getOriginalExtension()),
            'url' => request()->domain() . str_replace(Filesystem::getDiskConfig('public', 'root') . '/', '/', $this->completeFilePath),
            'dir_path' => $this->completeFileObjName ?:'',
            'create_time' => time(),
        ];
        SaveDb::trigger($this->tableName, $file_info);
        return [
            'save' => true,
            'msg' => '上传成功',
            'url' => $file_info['url'],
            'file_info' => $file_info,
        ];
    }
    /**
     * 删除本地文件
     * @return bool
     */
    public function del()
    {
        return parent::del();
    }
}