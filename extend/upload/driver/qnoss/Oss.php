<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------
namespace upload\driver\qnoss;

use upload\interfaces\OssDriver;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use Qiniu\Storage\BucketManager;

class Oss implements OssDriver
{

    protected static $instance;

    protected $accessKey;

    protected $secretKey;

    protected $bucket;

    protected $domain;

    protected $auth;

    public function __construct($config)
    {
        $this->accessKey = $config['qnoss_access_key'];
        $this->secretKey = $config['qnoss_secret_key'];
        $this->bucket = $config['qnoss_bucket'];
        $this->domain = $config['qnoss_domain'];
        $this->auth = new Auth($this->accessKey, $this->secretKey);
        return $this;
    }

    public static function instance($config)
    {
        if (is_null(self::$instance)) {
            self::$instance = new static($config);
        }
        return self::$instance;
    }

    public function save($objectName, $filePath)
    {
        $token = $this->auth->uploadToken($this->bucket);
        $uploadMgr = new UploadManager();
        list($result, $error) = $uploadMgr->putFile($token, $objectName, $filePath);
        if ($error !== null) {
            return [
                'save' => false,
                'msg'  => '保存失败',
            ];
        } else {
            return [
                'save' => true,
                'msg'  => '上传成功',
                'url'  => $this->domain . '/' . $result['key'],
            ];
        }
    }

    //删除单个文件
    public function del($objectName)
    {
        $config = new \Qiniu\Config();
        $bucketMgr = new BucketManager($this->auth, $config);
        $err = $bucketMgr->delete($this->bucket, $objectName);
        if ($err[1]) {
            return false;
        } else {
            return true;
        }
    }

}