<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\validate;

use think\Validate;

class Address extends Validate
{

    protected $rule = [
        'token' => 'require|max:25|token',
        'real_name' => 'require',
        'mobile' => 'require',
        'province' => 'require',
        'city' => 'require',
        'district' => 'require',
        'detail' => 'require',
        'city_id' => 'require',
//        'post_code'  => 'require',

    ];

    protected $message = [
        'token' => '表单禁止重复提交',
        'real_name' => '真实姓名必须填写',
        'mobile' => '手机号必须填写',
        'province' => '省份必须填写',
        'city' => '市必须填写',
        'district' => '区县必须填写',
        'detail' => '详细地址必须填写',
//        'post_code'  => '邮政编码必须填写',
        'city_id' => '城市区号必须填写',

    ];

    protected $scene = [
        'add' => ['real_name', 'mobile', 'province', 'city', 'district', 'detail', 'city_id'],
    ];
}