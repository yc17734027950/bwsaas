<?php

namespace buwang\jobs;

use app\common\model\member\WalletRecharge;
use buwang\base\BaseJob;
use buwang\facade\WechatPay;
use think\facade\Log;
use think\queue\Job;

/**
 * Class SystemCommonJob
 * @package buwang\jobs
 */
class SystemCommonJob extends BaseJob
{
    /**
     * 延时取消租户充值微信订单
     * @param Job $job
     * @param $order_sn
     * @return bool
     */
    public function doJob($order_sn): bool
    {
        try {
            $order =WalletRecharge::where('order_sn',$order_sn)->find();
            if(!$order) {
                Log::error('延时取消租户充值微信订单队列失败:');
                return false;
            }
            $wechat = WechatPay::doPay();
            //判断错误返回
            if (is_array($wechat)) {
                Log::error('延时取消租户充值微信订单队列失败1:'.$wechat['msg']);
                return false;
            }
            $result = $wechat->order->close($order_sn);
            if ($result) {
                //更新状态
                $order->state =2;
                $order->save();
                Log::error('处理成功'.json_encode($result));
            }
        } catch (\Throwable $e) {
            Log::error('延时取消租户充值微信订单队列失败2' . $e->getFile().$e->getLine().$e->getMessage());
        }

        return true;
    }

}
