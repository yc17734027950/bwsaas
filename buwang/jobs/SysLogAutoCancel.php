<?php

namespace buwang\jobs;

use buwang\base\BaseJob;
use app\common\model\SysLog;

/**
 * 系统日志清理任务
 * Class SysLogAutoCencel
 * @package buwang\jobs
 */
class SysLogAutoCancel extends BaseJob
{
    public function doJob($sysLog)
    {
        SysLog::destroy($sysLog['id']);
        return true;
    }

}
