<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\command;

use buwang\util\console\CliEcho;
use buwang\util\CommonTool;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use upload\driver\alioss\Oss;

class OssStatic extends Command
{

    protected function configure()
    {
        $this->setName('OssStatic')
            ->setDescription('将静态资源上传到oss上');
    }

    protected function execute(Input $input, Output $output)
    {
        $output->writeln("========正在上传静态资源到OSS上：========" . date('Y-m-d H:i:s'));
        $dir = root_path() . 'public' . DIRECTORY_SEPARATOR . 'static';
        $list = CommonTool::readDirAllFiles($dir);
        $uploadConfig = bw_config('alioss');
        $uploadPrefix = config('app.oss_static_prefix');
        foreach ($list as $key => $val) {
            list($objectName, $filePath) = [$uploadPrefix . DIRECTORY_SEPARATOR . $key, $val];
            try {
                $upload = Oss::instance($uploadConfig)
                    ->save($objectName, $filePath);
            } catch (\Exception $e) {
                CliEcho::error('文件上传失败：' . $filePath . '。错误信息：' . $e->getMessage());
                continue;
            }
            if ($upload['save'] == true) {
                CliEcho::success('文件上传成功：' . $filePath . '。上传地址：' . $upload['url']);
            } else {
                CliEcho::error('文件上传失败：' . $filePath . '。错误信息：' . $upload['msg']);
            }
        }
        $output->writeln("========已完成静态资源上传到OSS上：========" . date('Y-m-d H:i:s'));
    }

}