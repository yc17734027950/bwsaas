<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\traits;

use think\facade\View;

/**
 * 后台表单的通用crud
 * Trait CrudControllerTrait
 * @package buwang\traits
 */
trait CrudControllerTrait
{
    /**
     * 查看
     * @menu true
     */
    public function index()
    {
        if (request()->isAjax()) {
            $page = request()->get('page/d', 1);
            $limit = request()->get('limit/d', 10);

            //TODO 获取搜索条件
            //TODO 如果有不属于表内的字段,查询会报错
            $where = request()->get();
            unset($where['page']);
            unset($where['limit']);
            foreach ($where as $k => $v) {
                if (!$v && $v !== '0' && $v !== 0 && $v !== false) {
                    unset($where[$k]);
                }
            }

            try {
                $total = $this->model->where($where)->count();
                $list = $this->model->where($where)->order($this->model->getPk(), 'DESC')->page($page, $limit)->select();
            } catch (\Exception $e) {
                return $this->error('查询失败', ['errorMsg' => $e->getMessage()]);
            }

            $data = compact("total", 'list');
            return $this->success('successful', $data);
        }

        return view();
    }

    /**
     * 新增
     * @menu true
     */
    public function add()
    {
        if (request()->isPost()) {
            $param = request()->post();

            $this->model->startTrans();
            try {
                //TODO 新增记录
                $this->model->save($param);
                $this->model->commit();
            } catch (\Throwable $e) {
                $this->model->rollback();
                return $this->error('新增失败', ['errorMsg' => $e->getMessage()]);
            }
            return $this->success();
        }

        return view();
    }

    /**
     * 编辑
     * @menu true
     * @param int $id
     */
    public function edit($id = 0)
    {
        $row = $this->model->find($id);

        if (request()->isPost()) {
            if (!$row) return $this->error('记录不存在');

            $param = request()->post();
            //TODO 参数验证

            $this->model->startTrans();
            try {
                //TODO 更新记录
                $row->save($param);
                $this->model->commit();
            } catch (\Exception $e) {
                $this->model->rollback();
                return $this->error('编辑失败', ['errorMsg' => $e->getMessage()]);
            }
            return $this->success('编辑成功');
        }

        if (!$row) return $this->error_jump('记录不存在');

        View::assign('row', $row);
        return view();
    }

    /**
     * 删除
     * @menu true
     * @param int $ids 主键,删除多行时,拼接
     */
    public function del($ids = 0)
    {
        if (request()->isPost()) {
            if (!$ids) return $this->error('参数有误');

            $pk = $this->model->getPk();
            $list = $this->model->where($pk, 'in', $ids)->select();
            if (!$list) return $this->error('记录不存在');

            $this->model->startTrans();
            try {
                foreach ($list as $item) {
                    $item->delete();
                }

                $this->model->commit();
            } catch (\Exception $e) {
                $this->model->rollback();
                return $this->error('删除失败', ['errorMsg' => $e->getMessage()]);
            }
            return $this->success();
        }
    }
}