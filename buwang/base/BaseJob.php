<?php

namespace buwang\base;

use think\queue\Job;
use think\facade\Log;

/**
 * 队列任务基础类
 * Class BaseJob
 * @package buwang\base
 */
class BaseJob
{


    public function __construct()
    {
        $Dir = static::class;
        $index = strpos($Dir, "\\");
        $Dir = substr($Dir, $index + 1);
        $index = strpos($Dir, "\\");
        $Dir = substr($Dir, 0, $index);
        set_miniapp_database_prefix($Dir);
    }


    public function __call($name, $arguments)
    {
        $this->fire(...$arguments);
    }

    public function fire(Job $job, $data): void
    {
        try {
            $action = $data['do'] ?? 'doJob';//任务名
            $infoData = $data['data'] ?? [];//执行数据
            $errorCount = $data['errorCount'] ?? 0;//最大错误次数
            $log = $data['log'] ?? null;
            if (method_exists($this, $action)) {
                if ($this->{$action}(...$infoData)) {
                    //删除任务
                    $job->delete();
                    //记录日志
                    $this->info($log);
                } else {
                    if ($job->attempts() >= $errorCount && $errorCount) {
                        //删除任务
                        $job->delete();
                        //记录日志
                        $this->info($log);
                    } else {
                        //从新放入队列
                        $job->release();
                    }
                }
            } else {
                $job->delete();
            }
        } catch (\Throwable $e) {
            $job->delete();
            Log::error('执行消息队列发生错误,错误原因:' . $e->getMessage());
        }
    }

    protected function info($log)
    {
        try {
            if (is_callable($log)) {
                print_r($log() . "\r\n");
            } else if (is_string($log) || is_array($log)) {
                print_r($log . "\r\n");
            }
        } catch (\Throwable $e) {
            print_r($e->getMessage());
        }
    }

    public function failed($data, $e)
    {

    }
}