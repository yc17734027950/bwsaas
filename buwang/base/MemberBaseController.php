<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\base;

use app\common\model\MemberMiniapp;
use app\common\model\Miniapp;
use buwang\exception\MiniappException;
use buwang\traits\CrudControllerTrait;
use think\facade\View;
use app\manage\model\Member;
use buwang\service\MiniappService;

class MemberBaseController extends Manage
{
    //用户应用信息
    protected $member_miniapp;
    //用户应用id
    protected $member_miniapp_id;
    //应用id
    protected $miniapp_id;
    //应用信息
    protected $miniapp;


    // 初始化
    protected function initialize()
    {
        parent::initialize();
        $this->scopes = 'member';
        $this->miniapp_id = 0;
        //租户购买的应用id
        $member_app_id = request()->param('miniapp_id/d') ?: 0;
        //判断如果路由附加了参数bwsaas为true，就意味着必须传miniapp_id来标识租户操作哪个应用
        $bwsaas = $this->request->param('bwsaas/b');
        //页面未传递租户应用ID时，根据访问的应用目录获取租户应用ID
        if (!$member_app_id) {
            $dir = app('http')->getName();
            $member_app_id = MemberMiniapp::where('member_id', $this->uid)->where('miniapp_id', function ($query) use ($dir) {
                $query->name('miniapp')->where('dir', $dir)->where('status', '1')->field('id');
            })->value('id') ?: 0;
        }
        if ($member_app_id || $bwsaas) {
            if (!$this->uid) throw new MiniappException('此操作必须进行登录，登录信息验证失败', 401);
            //获取租户所属应用信息
            $this->member_miniapp = MemberMiniapp::where('member_id', $this->uid)->where('id', $member_app_id)->find();
            //是否已购买
            if (!$this->member_miniapp) throw new MiniappException('未找到所属应用,请先开通应用');

            $this->member_miniapp_id = $this->member_miniapp['id'];
            //TODO 购买的应用是否已过期

            $this->miniapp = $this->member_miniapp->miniapp;
            $this->miniapp_id = $this->miniapp['id'] ?: 0;
            View::assign('miniapp', $this->miniapp);
            View::assign('member_miniapp', $this->member_miniapp);
            View::assign('member_miniapp_id', $this->member_miniapp_id);
        }
    }

    protected function isMemberAppAuth($code = 401)
    {
        if (!$this->miniapp) throw new MiniappException('获取系统应用信息失败');
        if (!$this->member_miniapp) throw new MiniappException('获取所属应用信息失败');
        MiniappService::checkMiniappExpire($this->member_miniapp); //校验应用是否过期

    }

    /**
     * 当前租户是否有应用操作权限
     * @param $member_miniapp_id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function isValidMember($member_miniapp_id)
    {
        //获取租户应用信息
        $member_miniapp = MemberMiniapp::find($member_miniapp_id);
        if(!$member_miniapp) {
            throw new MiniappException('未找到应用');
        }
        if($member_miniapp['member_id'] !== $this->user->top_id) {
            throw new MiniappException('无应用操作权限');
        }
    }
}