<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------
// | Desc: 公众号开发默认继承基类
// +----------------------------------------------------------------------

namespace buwang\base;

use app\common\model\MemberMiniapp;
use buwang\exception\MiniappException;
use think\exception\HttpResponseException;
use think\facade\View;
use encrypter\Encrypter;

class Official extends BaseController{

    protected $bw_member_app;//租户应用信息
    protected $bw_member_app_id;//访问租户应用ID
    protected $service_id;//访问租户应用客户端ID
    protected $web_config;//站点基本配置参数

    /**
     * 初始化类
     */
    protected function initialize(){
        parent::initialize();
        //读取租户应用信息
        $this->bw_member_app  = self::memberMiniappAuth();
        if(!$this->bw_member_app){
            throw new MiniappException("访问公众号服务出错");
        }
        //判断是否授权登录
        $this->user = app('userService')->getUser();//从session取用户数据(重写基类里面的$this->user)
        if(!$this->user){
            $response = redirect((string)url('api/official/auth', ['app' => $this->bw_member_app['id'],'url' => Encrypter::bwEncode(self::backUrl())], false, false), 301)->header([]);
            throw new HttpResponseException($response);
        }
        $invite_code = app('userService')->setInviteCode();   //服务端缓存邀请码【网址里参数invite_code】
        //设置常用参数
        $this->bw_member_app_id     = $this->bw_member_app['id'];
        $this->service_id            = $this->bw_member_app['service_id'];
        $this->web_config            = bw_config('web_config'); //当前站点配置
        //传参数到模板调用
        $assign['user']              = $this->user;
        $assign['invite_code']       = $invite_code ? $invite_code:'';//推荐人的推荐码
        $assign['web_config']        = $this->web_config;
        $assign['bw_member_app']           = $this->bw_member_app;
        $assign['bw_member_app_id']          = $this->bw_member_app_id;
        // 模板全局变量赋值
        View::assign($assign);
    }

    /**
     * 记录后退网址
     * @return array|mixed|string|\think\string
     */
    protected function backUrl(){
        $url     = input('backurl','','strip_tags');
        $referer = $this->request->header('referer');
        if (empty($url) && isset($referer)) {
            return $referer;
        }else{
            return empty($url) ? $this->request->url() : $url;
        }
    }

    /**
     * 接口认证 获取用户购买的应用的信息
     * @return array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function memberMiniappAuth() {
        $app = $this->request->param('service_id/s',0);
        if(!$app) $app= session('service_id');
        if(!$app) throw new MiniappException("参数service_id错误");
        session('service_id', $app);//$app存在 设置存储下service_id
        return MemberMiniapp::where(['service_id' => $app,'is_lock' => 0])->field('id,appname,service_id,create_time,update_time,mp_appid,miniapp_appid')->cache(360)->find();
    }
}
