<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\middleware;

use buwang\interfaces\MiddlewareInterface;
use buwang\traits\JsonTrait;

/**
 * API接口的签名验证
 * Class SignCheck
 * @package buwang\middleware
 */
class SignCheck implements MiddlewareInterface
{
    use JsonTrait;

    /**
     * @param \app\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        //TODO 规划做API接口的签名验证
        return $next($request);
    }
}
