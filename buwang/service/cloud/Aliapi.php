<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\service\cloud;

use app\manage\model\common\aliapi\Service;
use app\Request;
use buwang\traits\ErrorTrait;

class Aliapi
{
    //应用id
    protected $miniapp_id;
    //租户id
    protected $member_id;
    //租户应用id
    protected $member_miniapp_id;

    use ErrorTrait;

    public function __construct(Request $request)
    {
        $this->miniapp_id = 4;
        $this->member_id = 2;
        $this->member_miniapp_id = 4;
    }

    public function run(string $file_name, array $param): bool
    {
        //TODO 服务类是否存在

        //TODO 服务类是否存在run方法
        //TODO 服务是否已安装
        $service = Service::where(['file_name' => $file_name])->find();
        if (!$service) return self::setError('服务尚未开放');

        //TODO 该租户 该服务 是否有剩余次数

    }
}