<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace buwang\service\cloud;

class Ip extends Base
{
    protected $base_uri = 'https://ipquery.market.alicloudapi.com';

    public function run(array $param)
    {
        //请求参数
        $uri = "/query?ip={$param['ip']}";

        //Guzzle Http遇到非200返回码会抛出异常,需要进行捕获
        try {
            $res = json_decode($this->client->request($this->method, $uri, $this->options)->getBody()->getContents(), true);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $err = json_decode($e->getResponse()->getBody()->getContents(), true);

            if ($err['ret'] == 400) return self::setError('ip参数不正确');
            else return self::setError('服务异常');
        }

        return $res['data'];
    }
}