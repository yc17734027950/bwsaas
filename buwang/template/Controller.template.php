
namespace <?php echo $controller_namespace?>;

use buwang\base\AdminBaseController;
use buwang\traits\CrudControllerTrait;

class <?php echo $controller_class_name?> extends AdminBaseController
{
    use CrudControllerTrait;

    protected $model = null;//模型实例

    protected function initialize()
    {
        parent::initialize();

        $this->model = new <?php echo $model_name?>();
    }
<?php if($temp_class_name_str){?>
    /**
    * 查看
    */
    public function index()
    {
        if(request()->isAjax()){
            $page = request()->get('page/d', 1);
            $limit = request()->get('limit/d', 10);

            //TODO 获取搜索条件
            //TODO 如果有不属于表内的字段,查询会报错
            $where = request()->get();
            unset($where['page']);
            unset($where['limit']);
            foreach ($where as $k => $v) {
                if(!$v && $v !== '0' && $v !== 0 && $v !== false) {
                    unset($where[$k]);
                }
            }

            try {
                $total = $this->model->with(<?php echo $temp_class_name_str?>)->where($where)->count();
                $list = $this->model->with(<?php echo $temp_class_name_str?>)->where($where)->order($this->model->getPk(), 'DESC')->page($page, $limit)->select();
            }catch (\Exception $e){
                $this->error('查询失败', ['errorMsg'=>$e->getMessage()]);
            }

            $data = compact("total", 'list');
            $this->success('success', $data);
        }
        return view();
    }
<?php }?>
}
