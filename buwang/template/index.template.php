{extend name="../../../view/public/base" /}

{block name="css"}
<style>
    body {
        background-color: #F2F2F2;
    }
</style>
{/block}

{block name="body"}
<div style="padding: 20px; background-color: #F2F2F2;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">

            <!--搜索区域-->
            <div class="layui-card">
                <div class="layui-collapse" lay-accordion="" lay-filter="search">
                    <div class="layui-colla-item">
                        <h2 class="layui-colla-title">查询条件</h2>
                        <div class="layui-colla-content">
                            <form id="searchForm" class="layui-form" lay-filter="searchForm" autocomplete="off">

                                <div class="layui-form-item layui-inline">
                                    <label class="layui-form-label"><?php echo $pk_comment?></label>
                                    <div class="layui-input-inline">
                                        <input type="number" name="<?php echo $pk?>" placeholder="请输入<?php echo $pk_comment?>"
                                               class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button class="layui-btn layui-btn-normal" type="button" lay-submit lay-filter="searchBtn">
                                            搜索
                                        </button>
                                        <button class="layui-btn layui-btn-primary" type="button" onclick="resetForm()">
                                            重置
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!--列表区域-->
            <div class="layui-card" style="background-color: #F2F2F2;">
                <table id="list" lay-filter="listFilter"></table>

                <!--工具栏-->
                <script type="text/html" id="listToolbar">
                    <a class="layui-btn layui-btn-sm layui-bg-black" lay-event="refresh" title="刷新"><i class="layui-icon layui-icon-refresh" style="font-size: 8px !important;"></i> </a>
                    <a class="layui-btn layui-btn-sm layui-btn-normal" lay-event="add" title="新增">新增</a>
                    <a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="del" title="删除">批量删除</a>
                </script>

<?php echo $cols_template?>

                <!--状态开关-->
                <script type="text/html" id="listBar">
                    <a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="edit">编辑</a>
                    <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del">删除</a>
                </script>
            </div>
        </div>
    </div>
</div>

{/block}

{block name="js"}
<script>
    let element, layer, form, table, bwajax, $;
    let ajax_index = "{:Url('index')}";
    let ajax_edit = "{:Url('edit')}";
    let ajax_add = "{:Url('add')}";

    layui.use(['element', 'layer', 'form', 'table', 'bwajax'], function () {
        element = layui.element;
        layer = layui.layer;
        form = layui.form;
        table = layui.table;
        bwajax = layui.bwajax.instance();
        $ = layui.jquery;

        //执行列表
        table.render({
            elem: '#list',
            url: ajax_index,
            response: {
                statusCode: 200,  //规定成功的状态码，默认：0
            },
            parseData: function (res) { //res 即为原始返回的数据
                return {
                    "code": res.code, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": res.data.data.total, //解析数据长度
                    "data": res.data.data.list //解析数据列表
                };
            },
            toolbar: '#listToolbar',
            cols: [[
                {type:'checkbox', fixed: 'left'},
<?php echo $cols?>
                {title: '操作', toolbar: '#listBar', align: 'center', fixed: "right", width: 140}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 10,
            page: true
        });

        //监听表格头部工具栏事件
        table.on('toolbar(listFilter)', function (obj) {
            var checkStatus = table.checkStatus(obj.config.id);
            switch (obj.event) {
                case 'refresh':
                    reload();
                    break;
                case 'add':
                    bwOpenIframe(ajax_add, '新增');
                    break;
                case 'del':
                    var checkStatus = table.checkStatus('list');
                    var data = checkStatus.data;

                    var idsArr = [];
                    data.forEach(function(value) {
                        idsArr.push(value.<?php echo $pk?>);
                    });
                    ids = idsArr.join(',');

                    if(!ids) layer.msg('没有选中行', {icon: 2});
                    else del(ids);

                    break;
            }
        });

        //监听工具条
        table.on('tool(listFilter)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）

            switch (layEvent) {
                case 'edit':
                    bwOpenIframe(ajax_edit + "?id=" + data.<?php echo $pk?>, '编辑');
                    break;
                case 'del':
                    del(obj.data.<?php echo $pk?>);
                    break;
<?php echo $tool_event?>
            }
        });

        form.on('submit(searchBtn)', function (data) {
            reload(1);

            return false;
        })

<?php echo $layui_js?>
    });
</script>
{/block}