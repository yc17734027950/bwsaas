<?php
return [
    [
        //角色名称/功能名称
        'name' => 'bwwechat基础功能',
        //角色唯一标识,不可重复【数据库唯一】
        'group_name' => 'bwwechat_base',
        //角色备注/功能描述
        'remark' => '',
        //功能类型:1=基础功能,2=附加功能[一个应用必须要有一个基础功能，附加功能不是必须的]
        'module_type' => '1',
        //功能价格【初始化数据，可以安装后后台进行修改】
        'module_price' => 0,
        //角色拥有的节点【权限节点数组】
        'nodes' => [
            [
                "title" => '主页',
                //菜单url【遵循TP框架的路由规则填写和路由文件定义的要一致】
                "menu_path" => '/bwwechat/admin/index',
                //实际【遵循TP框架的路由规则填写和路由文件定义的要一致】
                "name" => '/bwwechat/admin.Index/index',
                //权限标识,必填 唯一
                "auth_name" => '_bwwechat_admin_index',
                //附加参数 ?id=1&name=demo【生成的菜单会附加加参数】
                "param" => '',
                //打开方式【选项：_blank、_parent、_self、_top】
                "target" => '_self',
                //是否菜单 1=是,0=否[菜单会显示出来，节点会隐藏不显示]
                "ismenu" => '1',
                //图标【Font Awesome相关图标】
                "icon" => 'fa fa-file',
                //备注
                "remark" => '',
                //子节点或菜单
                'children' => []
            ]
        ]
    ]
];