<?php

return [
    'bw_page_html'      => '共 %s 条记录，每页显示 %s 条，共 %s 页当前显示第 %s 页。',
    //基础返回值
    'SUCCESS' => '操作成功' ,
    'FAIL' => '操作失败' ,
    'SAVE_SUCCESS' => '保存成功' ,
    'SAVE_FAIL' => '保存失败' ,
    'REQUEST_SUCCESS' => '请求成功' ,
    'REQUEST_FAIL' => '请求失败' ,
    'DELETE_SUCCESS' => '删除成功' ,
    'DELETE_FAIL' => '删除失败' ,
    'UNKNOW_ERROR' => '未知错误' ,
    'PARAMETER_ERROR' => '参数错误' ,
    'REQUEST_SITE_ID' => '缺少必须参数站点id' ,
    'REQUEST_APP_MODULE' => '缺少必须参数应用模块' ,
    //数据库
    'DABASE_REPAIR_FAIL' => '数据库修复失败' ,
    'DATABASE_OPTIMIZE_FAIL' => '数据表优化失败' ,
    'REQUEST_DATABASE_TABLE' => '请指定要选择的数据表' ,
];