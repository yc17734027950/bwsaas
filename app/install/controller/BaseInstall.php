<?php

namespace app\install\controller;

use think\facade\Cache;
use think\App;
use think\facade\View;
use buwang\traits\JsonTrait;
class BaseInstall
{
    use JsonTrait;
    public $replace;
    public $lock_file;
    public $lang;
    public $init_data;

    public function __construct()
    {
        //加载基础化配置信息
        if(!defined('__ROOT__')) define('__ROOT__', str_replace(['/index.php', '/install.php'], '', request()->root(true)));
        $this->replace   = [
            'INSTALL_CSS' =>  '/static/install/css',
            'INSTALL_IMG' =>  '/static/install/img',
            'INSTALL_JS'  =>  '/static/install/js',
        ];
        $this->lock_file = '../install.lock';//锁定文件
        $root_url        = __ROOT__;
        View::assign("root_url", $root_url);
        View::assign("INSTALL_JS", $this->replace['INSTALL_JS']);
        View::assign("INSTALL_CSS", $this->replace['INSTALL_CSS']);
        View::assign("INSTALL_IMG", $this->replace['INSTALL_IMG']);
    }

    /**
     * 操作成功返回值函数
     * @param string $data
     * @param string $code_var
     * @return \think\Response
     */
    public function returnSuccess($data = '', $code_var = 'SUCCESS')
    {
        $lang_array = $this->getLang();
        $lang_var   = isset($lang_array[$code_var]) ? $lang_array[$code_var] : $code_var;

        if ($code_var == 'SUCCESS') {
            $code_var = 0;
        } else {
            $code_array = array_keys($lang_array);
            $code_index = array_search($code_var, $code_array);
            if ($code_index != false) {
                $code_var = 10000 + $code_index;
            }
        }

        return $this->success($lang_var, $data, $code_var);
    }

    /**
     * 操作失败返回值函数
     * @param string $data
     * @param string $code_var
     * @return \think\Response
     */
    public function returnError($data = '', $code_var = 'FAIL')
    {
        $lang_array = $this->getLang();
        if (isset($lang_array[$code_var])) {
            $lang_var = $lang_array[$code_var];
        } else {
            $lang_var = $code_var;
            $code_var = 'FAIL';
        }
        $code_array = array_keys($lang_array);
        $code_index = array_search($code_var, $code_array);
        if ($code_index != false) {
            $code_var = -10000 - $code_index;
        }
        return $this->error($lang_var, $data, $code_var);
    }

    /**
     * 获取语言包数组
     */
    public function getLang()
    {
        $default_lang = config("lang.default_lang");
        $cache_common = Cache::get("lang_app/lang/" . $default_lang . '/model.php');
        if (empty($cache_common)) {
            $cache_common = include root_path() .'app/lang/' . $default_lang . '.php';
            Cache::tag("lang")->set("lang_app/lang/" . $default_lang, $cache_common);
        }
        $lang_path = isset($this->lang) ? $this->lang : '';
        if (!empty($lang_path)) {
            $cache_path = Cache::get("lang_" . $lang_path . "/" . $default_lang . '/model.php');
            if (empty($cache_path)) {
                $cache_path = include root_path() .$lang_path . "/" . $default_lang . '.php';
                Cache::tag("lang")->set("lang_" . $lang_path . "/" . $default_lang, $cache_path);
            }
            $lang = array_merge($cache_common, $cache_path);
        } else {
            $lang = $cache_common;
        }
        return $lang;
    }

}