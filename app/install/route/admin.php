<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

//快捷访问
use think\Response;
use think\facade\Route;
Route::group(function () {
    Route::rule('', 'Index/index');
    Route::rule('Index/index', 'Index/index');
    Route::rule('Index/testdb', 'Index/testdb');//检测数据库连接
    Route::rule('Index/testrdb', 'Index/testrdb');//检测redis数据库连接
    Route::rule('Index/process', 'Index/process');//执行过程
    Route::rule('Index/installSuccess', 'Index/installSuccess');//安装成功


})->append(['bwsaas' => 1]);
/**
 * miss 路由
 */
Route::miss(function () {
    if (app()->request->isOptions()) {
        $header = config('cookie.header');
        $header['Access-Control-Allow-Origin'] = app()->request->header('origin');
        return Response::create('ok')->code(200)->header($header);
    }

    return Response::create()->code(403)->content('没有找到路由地址');


});
