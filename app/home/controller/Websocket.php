<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\home\controller;

use buwang\base\Home;
use websocket\Client;

class Websocket extends Home
{
    public function index()
    {


        $client = new Client("ws://129.211.45.16:9501");
        $client->send("Hello WebSocket.org!");

        echo $client->receive(); // Will output 'Hello WebSocket.org!'
    }

    public function serve()
    {
        $GLOBALS['PHPUNIT_COVERAGE_DATA_DIRECTORY'] = dirname(dirname(__FILE__)) . '/build/tmp';

    }


}
