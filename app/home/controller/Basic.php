<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\home\controller;

use buwang\base\Manage;
use think\facade\View;

class Basic extends Manage
{
    /**
     * 模板布局, false取消
     * @var string|bool
     */
    protected $layout = true;
    protected $footer = 1;

    protected function initialize()
    {
        parent::initialize();
        View::assign('footer', $this->footer);
        $this->layout && $this->app->view->engine()->layout($this->layout);
    }
}
