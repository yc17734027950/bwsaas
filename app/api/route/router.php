<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

//快捷访问
use think\Response;
use think\facade\Route;

/**
 * 不需要登录，需要解析token的场景（如登录后有更多定制的功能）
 */
Route::group('v1', function () {
    //小程序用户登录
    Route::post('login', 'v1.MiniProgram/login');
    // 获取小程序的配置信息
    Route::get('config', 'v1.MiniProgram/config');
    //公众号 h5 APP通用的api登录接口
    Route::post('commonLogin', 'v1.App/commonLogin');
    //公众号 h5 APP通用的api注册接口
    Route::post('commonReg', 'v1.App/commonReg');
    //小程序绑定公众号
    Route::get('bindOfficial', 'v1.MiniProgram/bindOfficial');
    //API接口微信公众号授权登录
    Route::get('officialLogin', 'v1.App/officialLogin');
    //API接口用户退出登录
    Route::post('loginOut', 'v1.User/loginOut');
    //头条系小程序登录
    Route::post('ttLogin', 'v1.App/ttLogin');
    //API方式获取重写的tp验证码和session_id
    Route::get('getVerify', 'v1.App/getVerify');



    //API接口微信公众号JSSDK参数
    Route::get('getJSSDK', 'v1.App/getJSSDK');
    Route::get('haveBindMobile', 'v1.MiniProgram/haveBindMobile');//是否绑定了手机号
    Route::get('user/getCodeUser', 'v1.User/getCodeUser');//获取推荐码对应的用户
    Route::post('user/setPassword', 'v1.User/setPassword');//重置用户密码

    //短信发送
    Route::group('sms', function () {
        Route::post('send', 'v1.Sms/send');
    });
    //小程序直播列表
    Route::get('live', 'v1.MiniProgram/live');
})->middleware(buwang\middleware\Login::class, false);

/**
 * 不需要登录，需要解析token的场景（如登录后有更多定制的功能）
 */
Route::group(function () {
    //服务端渲染SSR微信公众号授权登录
    Route::get('official/auth', 'wechat.wechatAuthMp/auth');
    //租户扫码登录绑定Openid
    Route::get('official/bindQrcodeOpenid', 'wechat.WechatAccount/bindQrcodeOpenid');
    //微信公众号授权回调
    Route::get('official/authCallback', 'wechat.wechatAuthMp/authCallback');
    //公众号自动授权实例
    Route::get('home/Index', 'home.Index/index');
    //余额充值套餐
    Route::get('v1/user/moneySelect', 'v1.User/moneySelect');
    //余额充值套餐
    Route::get('v1/city', 'v1.User/cityList');
    //余额充值套餐
    Route::get('v1/city/2', 'v1.User/cityListTwo');

})->middleware(buwang\middleware\Login::class, false);

/**
 * 需要强制登录的接口
 */
Route::group(function () {
    // 通用前台各端上传接口
    Route::post('v1/upload', 'v1.User/upload');
    // 获取小程序码
    Route::post('v1/getProgramCode', 'v1.MiniProgram/getProgramCode');
    // 获取小程序二维码
    Route::post('v1/getProgramQrCode', 'v1.MiniProgram/getProgramQrCode');

    // 绑定小程序端获取到的手机号
    Route::post('v1/bindWechatMobile', 'v1.MiniProgram/bindWechatMobile');
    // 获取默认地址
    Route::get('v1/user/getDefaultAddress', 'v1.User/getDefaultAddress');
    // 获取地址列表
    Route::get('v1/user/getAddressList', 'v1.User/getAddressList');
    // 地址删除
    Route::post('v1/user/delAddress', 'v1.User/delAddress');
    // 获取user用戶信息
    Route::get('v1/user/getUserInfo', 'v1.User/getUserInfo');
    // 保存或修改我的地址
    Route::post('v1/user/addAddress', 'v1.User/addAddress');
    //快捷设置默认地址
    Route::post('v1/user/setDefaultAddress', 'v1.User/setDefault');
    // 验证是否设置安全密码
    Route::get('v1/user/isSafePassword', 'v1.User/isSafePassword');
    // 检查旧的安全密码
    Route::post('v1/user/checkSafePassword', 'v1.User/checkSafePassword');
    // 设置安全密码6位数字
    Route::post('v1/user/setSafePassword', 'v1.User/setSafePassword');
    // 绑定手机号
    Route::post('v1/user/bindPhoneNumber', 'v1.User/bindPhoneNumber');
    //余额充值
    Route::rule('v1/user/rechargeMoney', 'v1.User/rechargeMoney');
    //充值记录
    Route::get('v1/user/rechargeList', 'v1.User/rechargeList');
    //账单
    Route::get('v1/userBill/:type', 'v1.User/billList');//用户账单

    // 提现绑定状态
    Route::get('v1/extract/bankInfo', 'v1.Extract/bankInfo');
    // 提现绑定
    Route::post('v1/extract/bankInfo', 'v1.Extract/bankInfo');
    // 提现参数
    Route::get('v1/extract', 'v1.Extract/extract');
    // 发起提现申请
    Route::post('v1/extract', 'v1.Extract/extract');
    //提现列表
    Route::get('v1/extract/list', 'v1.Extract/extractList');


})->middleware(buwang\middleware\Login::class, true);

/**
 * 需要强制登录的接口
 */
Route::group('v1', function () {
        //站内信
        Route::group('message', function () {
            Route::get('index', 'v1.Message/index');
            Route::post('read/:id', 'v1.Message/read');
            Route::post('read/all', 'v1.Message/readAll');//全部已读
        });
})->middleware(buwang\middleware\Login::class, true);

//微信开放平台授权
Route::rule('wechat_auth/ticket', 'wechat.wechatAuthOpen/ticket');
Route::rule('wechat_auth/:appid/server', 'wechat.wechatAuthOpen/server');

//微信公众号授权登录
Route::rule('wechat_auth/createScanCode', 'wechat.wechatAccount/createScanCode');
Route::rule('wechat_auth/getLoginState', 'wechat.wechatAccount/getLoginState');

//充值余额回调
Route::rule('notifyMini/:appid', 'v1.Notify/rechargeMini')->completeMatch(true);
Route::rule('notifyOfficial/:appid', 'v1.Notify/rechargeOfficial')->completeMatch(true);
Route::rule('notifyNative/:appid', 'v1.Notify/rechargeNative')->completeMatch(true);//扫码支付回调（模式2）

//查询应用版本信息
Route::get('app/version', 'cloud.MiniappCloud/appVersionInfo');

//参数过滤
Route::pattern([
    'app' => '\d+',
    'appid' => '\w+',
    'id' => '\d+',
    'version' => '[0-9a-zA-Z]+',
    'module' => '[a-zA-Z]+',
    'controller' => '[a-zA-Z]+',
    'action' => '[a-zA-Z]+'
]);

/**
 * miss 路由
 */
Route::miss(function () {
    if (app()->request->isOptions()) {
        $header = config('cookie.header');
        $header['Access-Control-Allow-Origin'] = app()->request->header('origin');
        return Response::create('ok')->code(200)->header($header);
    }
    return Response::create()->code(403)->content('没有找到路由地址');

});
