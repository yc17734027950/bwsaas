<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\api\controller\cloud;



/**
 * bwsaas应用云服务接口
 */
class MiniappCloud extends Basic
{

    /**
     * 应用最新版本信息查询接口
     */
    public function appVersionInfo(){
        $dir = $this->request->param('dir'); //应用
        if($dir!='bwmall') return $this->error('当前应用云端暂无更新');
        return $this->success('查询成功',$this->service->getNewUnpdate($dir)); //查询
    }

}