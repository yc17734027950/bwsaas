<?php

namespace app\manage\job;

use buwang\base\BaseJob;

/**
 * Demo消息队列
 * Class DemoJob
 * @package app\manage\job
 */
class DemoJob extends BaseJob
{
    public function doJob($a, $b)
    {
        //自行开启事务

        if (0) {//队列执行失败时
            return false;
        }

        return true;
    }
}
