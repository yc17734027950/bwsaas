<?php

/**
 * 无限极分类,根据子类id 找所有父类
 */
if (!function_exists('infinite_get_parent')) {
    function infinite_get_parent($data, $son_id, $level=0, $isClear=true){
        //声明一个静态数组存储结果
        static $res = array();
        //刚进入函数要清除上次调用此函数后留下的静态变量的值，进入深一层循环时则不要清除
        if($isClear==true) $res =array();
        foreach ($data as $v) {
            if($v['id'] == $son_id){
                $v['level'] = $level;
                $res[] = $v;
                infinite_get_parent($data, $v['parent_id'], $level-1, $isClear=false);
            }
        }
        return $res;
    }
}

/**
 * 无限极分类,根据父类id找所有子类
 */
if (!function_exists('infinite_get_son')) {
    function infinite_get_son($data, $p_id=0, $level=0, $isClear=true){
        //声明一个静态数组存储结果
        static $res = array();
        //刚进入函数要清除上次调用此函数后留下的静态变量的值，进入深一层循环时则不要清除
        if($isClear==true) $res =array();
        foreach ($data as $v) {
            if($v['parent_id'] == $p_id){
                $v['level'] = $level;
                $res[] = $v;
                infinite_get_son($data, $v['id'], $level+1, $isClear=false);
            }
        }
        return $res;
    }
}

