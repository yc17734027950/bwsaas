<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\model;

use buwang\base\BaseModel;
use buwang\traits\JwtTrait;
use buwang\exception\MiniappException;

/**配置分类表
 * Class BwAuthNode
 * @package app\manage\model
 */
class ConfigTab extends BaseModel
{
    protected $pk = 'id';
    // 表名
    protected $table = 'bw_sys_config_tab';
    protected $updateTime = '';

    protected $deleteTime = '';


    /*
* 设置查询初始化条件
* @param string $alias 表别名
* @param object $model 模型实例化对象
* @return object
* */
    public static function valiWhere($alias = '', $model = null, $dir = '', $show = true)
    {

        if ($dir) {
            $model = is_null($model) ? Config::getAppConfigTabModel($dir) : $model;
            if (!$model) throw new MiniappException("该应用配置分类表缺失，请创建{$dir}的配置分类表");
        } else {
            $model = is_null($model) ? new self() : $model;
        }

        if ($alias) {
            $model = $model->alias($alias);
            $alias .= '.';
        }
        if ($show) $model = $model->where("{$alias}is_show", 1);
        return $model;
    }


    /**
     * 判断是否存在配置
     */
    public static function haveConfig($id, $app = '')
    {
        return Config::valiWhere('', null, $app, false)->where('tab_id', $id)->count();
    }


}
