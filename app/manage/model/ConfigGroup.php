<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\model;

use buwang\base\BaseModel;
use buwang\exception\MiniappException;

/**  配置组
 * Class ConfigGroup
 * @package app\manage\model
 */
class ConfigGroup extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'sys_config_group';


    public static function getTypeName($type)
    {
        $rs = '';
        switch ($type) {
            case 'text':
                $rs = '普通文本框';
                break;
            case 'textarea':
                $rs = '多行文本域';
                break;
            case 'r_textarea':
                $rs = '富文本域';
                break;
            case 'radio':
                $rs = '单选框';
                break;
            case 'checkbox':
                $rs = '复选框';
                break;
            case 'select':
                $rs = '下拉框';
                break;
            case 'upload':
                $rs = '文件上传';
                break;
        }
        return $rs;

    }

    public function configGroupData()
    {
        return $this->hasMany(ConfigGroupData::class, 'group_id');
    }

    /**
     * 获取配置
     */
    public static function getByName($name)
    {
        //返回数据
        $data = array();
        $group = self::where('config_name', $name)->find();
        $data = $group->configGroupData()->select()->toArray();
        return $data;
    }


    public static function valiWhere($alias = '', $model = null, $dir = '')
    {
        if ($dir) {
            $model = is_null($model) ? ConfigGroupData::getAppGroupModel($dir) : $model;
            if (!$model) throw new MiniappException("该应用組合分類表缺失，请创建{$dir}的組合分類表");
        } else {
            $model = is_null($model) ? new self() : $model;
        }
        if ($alias) {
            $model = $model->alias($alias);
            $alias .= '.';
        }
        return $model;
    }
}