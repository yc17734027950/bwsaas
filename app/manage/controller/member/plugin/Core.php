<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member\plugin;

use buwang\base\MemberBaseController;
use think\facade\View;
use app\common\model\MemberPluginOrder;

/**
 * @ControllerAnnotation(title="租户插件中心")
 */
class Core extends MemberBaseController
{
    use \buwang\traits\Crud;

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\Plugin();
    }


    /**
     * 组合插件中心页面
     */
    public function index()
    {
        $app = $this->request->get('app/s', '');
        //已启用插件列表
        if (request()->isPost()) {
            $type = ['member_system','all_system'];
            if($app)$type = ["member_{$app}"];
            //得到租户已购买插件列表
            $count = 0;
            $list = [];
            $plugin_ids = MemberPluginOrder::where('member_id', $this->user->top_id)->column('plugin_id');

            $list = $this->model->with('group')->where(['status' => 1])->where("type", 'in', $type)->order('id asc')->select();
            $list = array_map(
                function ($item)use(&$invalid,$plugin_ids){
                    if (in_array($item['id'], $plugin_ids))
                    {
                        $item['buy'] = true;
                    }else{
                        $item['buy'] = false;
                    }
                    $item['icon'] = $item['logo_image']?:'/static/bwmall/logo.png';
                    //得到插件配置数据
                    $item['info'] = get_addons_info($item['name']);
//                    var_dump($item['info']['url']);die;
                    $url =  (string)$item['info']['url'];
                    //取出插件url
                    $item['info']['url'] = $url;
                    return $item;
                },
                $list->toArray()
            );
            //过滤掉无菜单的插件
            $invalid = 0;
//            if ($plugin_ids) {
//                $list = $this->model->with('group')->where(['status' => 1])->where('id', 'in', $plugin_ids)->where("type", 'in', $type)->order('id asc')->select();
//                $count = $this->model->with('group')->where(['status' => 1])->where('id', 'in', $plugin_ids)->where("type", 'in', $type)->order('id asc')->count();
//                $list = array_map(
//                    function ($item)use(&$invalid){
////                        if($item['group']){
//                            $item['icon'] = $item['logo_image']?:'/static/bwmall/logo.png';
////                            $found_index = array_search('member', array_column($item['group'], 'scopes'));
////                            if($found_index !==false){
//                                return $item;
////                            }else{
////                                $invalid ++;
////                                return null;
////                            }
////                        }else{
////                            $invalid ++;
////                            return null;
////                        }
//                    },
//                    $list->toArray()
//                );
//            }
            //过滤掉无菜单插件
//            $list = array_filter($list,function($val){
//              return $val?true:false;
//            });
            $data = [
                'total' => $count - $invalid,
                'list' => $list,
            ];

            return $this->success('success', $data);
        }
        View::assign('app', $app);
        return view();
    }


    /**
     * 菜单渲染
     */
    public function menu()
    {
        $addons = $this->request->get('addons/s', '');
        $jump = $this->request->get('jump/s', 0);
        $menu_id = $this->request->get('menu_id/d', 0);
        $app = $this->request->get('app/s', '');//应用标识
        $menu = $this->model->getMemberMenu($this->user->top_id, $addons, $menu_id, $app);
        if (!$menu) return $this->error('该插件没有管理菜单');
        //跳转渲染
        if ($jump) {
            View::assign('menu_id', $menu_id);
            View::assign('list', $menu['list']);
            View::assign('menu', $menu['menu']);
            View::assign('url_info', $menu['url_info']);
            View::assign('app', $app);
            View::assign('addons', $addons);
            return view();
        }
        //得到菜单
        if (request()->isPost()) {
            return $this->success('得到菜单', ['list' => $menu]);
        }
        //查看插件是否存在菜单
        if (request()->isGet()) {
            //查询该插件是否存在总后台根节点
            return $this->success('存在菜单');
        }

    }

}