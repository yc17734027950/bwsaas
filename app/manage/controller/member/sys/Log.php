<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\member\sys;

use buwang\base\MemberBaseController;

//use EasyAdmin\annotation\ControllerAnnotation;
//use EasyAdmin\annotation\NodeAnotation;
//use think\App;
use app\manage\model\Member;

/**
 * @ControllerAnnotation(title="管理员操作记录表")
 */
class Log extends MemberBaseController
{
    use \buwang\traits\Crud;

    protected $relationSearch = true;

    public function initialize()
    {
        parent::initialize();
        $this->layout && $this->app->view->engine()->layout($this->layout);
        $this->model = new \app\common\model\SysLog();

        $this->assign('getAdminList', $this->model->getAdminList());

        $this->assign('getScopesList', $this->model->getScopesList());

    }


    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFieds')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            //得到该租户的所有日志
            $member_id = $this->user->top_id;
            $ids = Member::where('top_id', $member_id)->column('id');
            $count = $this->model
                ->withJoin('admin', 'LEFT')
                ->where($where)
                ->where('member_id', 'in', $ids)
                ->where('scopes', 'member')
                ->count();
            $list = $this->model
                ->withJoin('admin', 'LEFT')
                ->where($where)
                ->where('scopes', 'member')
                ->where('member_id', 'in', $ids)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'total' => $count,
                'list' => $list,
            ];
            return $this->success('success', $data);
        }
        return $this->fetch();
    }
}