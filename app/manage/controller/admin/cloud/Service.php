<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\manage\controller\admin\cloud;

use buwang\base\AdminBaseController;
use buwang\traits\CrudControllerTrait;
use buwang\util\File;

class Service extends AdminBaseController
{
    use CrudControllerTrait;

    protected $model = null;//模型实例

    protected function initialize()
    {
        parent::initialize();

        $this->model = new \app\common\model\cloud\Service();
    }
    /**
     * 查看
     * @menu true
     */
    public function index()
    {
        if (request()->isAjax()) {
            $page = request()->get('page/d', 1);
            $limit = request()->get('limit/d', 10);

            //TODO 获取搜索条件
            //TODO 如果有不属于表内的字段,查询会报错
            $where = request()->get();
            unset($where['page']);
            unset($where['limit']);
            foreach ($where as $k => $v) {
                if (!$v && $v !== '0' && $v !== 0 && $v !== false) {
                    unset($where[$k]);
                }
            }

            try {
                $total = $this->model->where($where)->count();
                $list = $this->model->where($where)->order($this->model->getPk(), 'DESC')->page($page, $limit)->select();
                /*添加日志跳转链接字段*/
                foreach($list as $key => &$value) {
                    $value['log_url'] = '/manage/admin/cloud/log/'.strtolower($value['file_name']).'Log/index';
                }
            } catch (\Exception $e) {
                return $this->error('查询失败', ['errorMsg' => $e->getMessage()]);
            }

            $data = compact("total", 'list');
            return $this->success('successful', $data);
        }

        return view();
    }

    /**
     * 新增
     * @menu true
     */
    public function add()
    {
        if (request()->isPost()) {
            $param = request()->post();
            $fileArr = File::get_dirs(root_path(DS.'buwang'.DS.'service'.DS.'cloud'));
            $param['file_name'] = (string) $param['file_name'];
            if(!$param['file_name']) return $this->error('缺少类名参数');
            if(in_array($param['file_name'],['Aliapi','Base','Cloud'])) return $this->error('类名与系统保留冲突');
            if(!in_array($param['file_name'].'.php',$fileArr['file'])) return $this->error('类名不存在，请注意大小写');
            if($this->model->where('file_name',$param['file_name'])->find()) return $this->error('服务已经存在，禁止再次新增');
            $this->model->startTrans();
            try {
                //TODO 新增记录
                $this->model->save($param);
                $this->model->commit();
            } catch (\Throwable $e) {
                $this->model->rollback();
                return $this->error('新增失败', ['errorMsg' => $e->getMessage()]);
            }
            return $this->success();
        }

        return view();
    }

    /**
     * 删除
     * @menu true
     * @param int $ids 主键,删除多行时,拼接
     */
    public function del($ids = 0)
    {
        if (request()->isPost()) {
            if (!$ids) return $this->error('参数有误');

            $pk = $this->model->getPk();
            $list = $this->model->where($pk, 'in', $ids)->select();
            if (!$list) return $this->error('记录不存在');

            //TODO 查询该服务租户是否还有剩余次数
            $res = \app\common\model\cloud\Wallet::where('amount', '>', 0)->where('service_id', 'in', $ids)->field('id')->select()->toArray();
            if (!empty($res)) return $this->error('租户还有剩余使用次数,无法删除');

            $this->model->startTrans();
            try {
                foreach ($list as $item) {
                    $item->delete();
                }

                $this->model->commit();
            } catch (\Exception $e) {
                $this->model->rollback();
                return $this->error('删除失败', ['errorMsg' => $e->getMessage()]);
            }
            return $this->success();
        }
    }
}
