<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\middleware;

use think\Request;

/**
 * 跨域处理
 * Class Auth
 * @package app\admin\middleware
 */
class Cross
{
    /**
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $Origin = $request->header('Origin');
        //$Headers = $request->header('Access-Control-Request-Headers');
        header('Access-Control-Allow-Origin: ' . $Origin);
        header('Access-Control-Allow-Headers: request-app, scopes, request-time, session_id, token, Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With');
        header('Access-Control-Allow-Credentials:true');
        header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1728000');//options请求缓存时间,2天
        header('Content-Type:text/plain charset=UTF-8');
        if (strtoupper($request->method()) == "OPTIONS") {
            header('HTTP/1.0 204 No Content');
            header('Content-Length:0');
            header('status:204');
            exit();
        }
        return $next($request);
    }
}
