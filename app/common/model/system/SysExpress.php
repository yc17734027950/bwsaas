<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model\system;

use buwang\traits\ModelTrait;
use buwang\base\BaseModel;

/**
 * Class Express
 * @package app\admin\model\system
 */
class SysExpress extends BaseModel
{
    use ModelTrait;

    public function getIsShowList()
    {
        return ['0'=>'取消','1'=>'展示',];
    }


    public static function systemPage($params)
    {
        $model = new self;
        if ($params['keyword'] !== '') $model = $model->where('name|code', 'LIKE', "%$params[keyword]%");
        $model = $model->order('sort DESC,id DESC');
        return self::page($model, $params);
    }
}