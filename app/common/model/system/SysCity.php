<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model\system;

use buwang\traits\ModelTrait;
use buwang\base\TimeModel;
/**
 * 全国地址model
 */
class SysCity extends TimeModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    use ModelTrait;

    protected $deleteTime = false;

    public function getIsShowList()
    {
        return ['1'=>'是','0'=>'否',];
    }
    /**
     * 获取子集分类查询条件
     * @return \think\model\relation\HasMany
     */
    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'city_id')->order('id ASC');
    }

    /**
     * 截取行政区划码得到城市编码
     */
    public static function getCode($code)
    {
        return substr($code, 0, 6);
    }
}
