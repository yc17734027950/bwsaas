<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\TimeModel;

class SysNotice extends TimeModel
{

    protected $name = "sys_notice";

    protected $deleteTime = false;


    /**分类
     * @return mixed
     */
    public function noticeCategory()
    {
        return $this->belongsTo('\app\common\model\SysNoticeCategory', 'cate_id', 'id');
    }

    public function getCategoryList()
    {
        return \app\common\model\SysNoticeCategory::where('name', '<>', 'news')->order('weight desc, id desc')->column('name', 'id');
    }


    /**
     * 得到租户系统通知
     */
    public static function getNoticeList($page = 1, $limit = 10, $where = [], $sort = 'add_time desc')
    {
        $data = [];
        $data['list'] = self::where($where)->page($page, $limit)->order($sort)->select()->toArray();
        foreach ($data['list'] as &$value) {
            $value['add_time'] = date('Y-m-d H:i:s', $value['add_time']);
        }

        $data['total'] = self::where($where)->count();
        return $data;
    }

}