<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model\member;

use buwang\base\BaseModel;

class WalletBill extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'member_wallet_bill';


    // 追加属性
    protected $append = [
        'type_text',
    ];


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : $value;
    }



    /**
     * @return \think\model\relation\BelongsTo
     * @noinspection PhpMethodParametersCountMismatchInspection
     */
    public function Member()
    {
        return $this->belongsto(\app\common\model\Member::class, 'member_id', 'id', [], 'LEFT');
    }


    /**得到租户明细类型
     * @return array
     */
    public function getTypeList()
    {
        return ['admin' => '系统变更', 'cloud_packet' => '购买云市场套餐','plugin'=>'插件服务','miniapp'=>'应用服务','recharge'=>'充值服务'];
    }

}