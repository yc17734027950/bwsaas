<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\TimeModel;

/**
 * 微信消息模板
 * Class WechatMessageTemplate
 * @package app\common\model
 */
class WechatMessageTemplate extends TimeModel
{
    protected $deleteTime = 'delete_time';

    public function getTypeList()
    {
        return ['0' => '小程序订阅消息', '1' => '公众号模板消息',];
    }

    public function getStatusList()
    {
        return ['0' => '关闭', '1' => '开启',];
    }

    /**
     * 获取小程序可用订阅消息模板ID
     * @param $member_miniapp_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getMessageTemplateList($member_miniapp_id){
        $param = [
            ['member_miniapp_id', '=', $member_miniapp_id],
            ['type', '=', 0],
            ['status', '=', 1],
        ];
        $template_list = self::where($param)->field('name, key, template_id')->select()->toArray();
        $list = [];
        array_walk($template_list, function($item) use (&$list){
            $list[$item['key']] = $item['template_id'];
        });
        return $list;
    }
}