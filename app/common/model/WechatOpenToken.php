<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;

class WechatOpenToken extends BaseModel
{

    protected $pk = 'id';

    /**
     * 添加编辑
     * @param array $param
     * @return WechatOpenToken|int|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function edit(array $param)
    {
        $data['authorizer_access_token'] = $param['access_token'];
        $data['authorizer_refresh_token'] = $param['refresh_token'];
        $data['expires_in'] = $param['expires_in'];
        $data['update_time'] = time();
        $miniapp = self::where(['member_miniapp_id' => $param['member_miniapp_id'], 'authorizer_appid' => $param['appid']])->find();
        if ($miniapp) {
            return self::where(['member_miniapp_id' => $param['member_miniapp_id'], 'authorizer_appid' => $param['appid']])->update($data);
        } else {
            $data['member_miniapp_id'] = $param['member_miniapp_id'];
            $data['authorizer_appid'] = $param['appid'];
            return self::insert($data);
        }
    }

    /**
     * 获取AccessToken
     * @param int $id
     * @param string $appid
     * @return array|false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function accessToken(int $id, string $appid)
    {
        $where['member_miniapp_id'] = $id;
        $where['authorizer_appid'] = $appid;
        $assess = self::where($where)->find();
        if ($assess) {
            $expires_in = time() - $assess['update_time'];
            if ($expires_in >= 6000) {
                return false;
            }
            return ['access_token' => $assess['authorizer_access_token'], 'appid' => $appid];
        }
        return false;
    }

    /**
     * 获取refreshToken
     * @param int $id
     * @param string $appid
     * @return array|false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function refreshToken(int $id, string $appid)
    {
        $where['member_miniapp_id'] = $id;
        $where['authorizer_appid'] = $appid;
        $assess = self::where($where)->find();
        if ($assess) {
            return ['refreshToken' => $assess['authorizer_refresh_token'], 'appid' => $appid];
        }
        return false;
    }
}
