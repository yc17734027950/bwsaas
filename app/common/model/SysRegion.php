<?php


namespace app\common\model;

use buwang\traits\ModelTrait;
use buwang\util\Util;

/**
 * 全国地址model
 */
class SysRegion extends \buwang\base\BaseModel
{
    use ModelTrait;


    /**
     * 得到城市数据
     */
    public static function getList()
    {
        $list = self::select()->toArray();
        return Util::tree($list);
    }
}