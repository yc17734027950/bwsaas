<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;
use app\common\model\member\WalletBill;

class Member extends BaseModel
{

    protected $pk = 'id';

    /**
     * 用户信息关联
     * @return \think\model\relation\HasOne|void
     */
    public function miniapp()
    {
        return $this->hasOne('MemberMiniapp', 'member_id', 'id');
    }

    /**
     * 用户钱包信息关联
     * @return \think\model\relation\HasOne|void
     */
    public function wallet()
    {
        return $this->hasOne(\app\common\model\member\Wallet::class, 'member_id', 'id')->bind(['money', 'freeze_money']);
    }

    /**
     * 用户数量统计(租户)
     */
    public static function addUserTotal($member_id)
    {
        $today = $yesterday = $week = 0;
        $member_miniapp_ids = MemberMiniapp::where('member_id', $member_id)->column('id');
        if (!$member_miniapp_ids) return compact('today', 'yesterday', 'week');
        //得到前七天的时间戳
        $weekBefor = date('Y-m-d', strtotime('-7 day'));
        $weekAfter = date('Y-m-d', strtotime('+1 day'));
        //查询昨天的用户数量
        $yesterday = User::where('member_miniapp_id', 'in', $member_miniapp_ids)->whereDay('create_time', 'yesterday')->count();
        //今天
        $today = User::where('member_miniapp_id', 'in', $member_miniapp_ids)->whereDay('create_time', 'today')->count();
        //前七天
        $week = User::where('member_miniapp_id', 'in', $member_miniapp_ids)->where('create_time', 'between time', [$weekBefor, $weekAfter])->count();
        return compact('today', 'yesterday', 'week');
    }

    /**
     * 用户提现统计
     */
    public static function extractTotal($member_id)
    {
        $today = $yesterday = $week = 0;
        $member_miniapp_ids = MemberMiniapp::where('member_id', $member_id)->column('id');
        if (!$member_miniapp_ids) return compact('today', 'yesterday', 'week');
        //得到前七天的时间戳
        $weekBefor = date('Y-m-d', strtotime('-7 day'));
        $weekAfter = date('Y-m-d', strtotime('+1 day'));
        //查询昨天的提现数量
        $yesterday = UserExtractLog::where('u.member_miniapp_id', 'in', $member_miniapp_ids)->alias('a')->join('user u', 'u.id=a.user_id', 'LEFT')->whereDay('a.createtime', 'yesterday')->where('a.status', '1')->sum('extract_price');
        //今天提现数量
        $today = UserExtractLog::where('u.member_miniapp_id', 'in', $member_miniapp_ids)->alias('a')->join('user u', 'u.id=a.user_id', 'LEFT')->whereDay('a.createtime', 'today')->where('a.status', '1')->sum('extract_price');
        //前七天提现数量
        $week = UserExtractLog::where('u.member_miniapp_id', 'in', $member_miniapp_ids)->alias('a')->join('user u', 'u.id=a.user_id', 'LEFT')->where('a.createtime', 'between time', [$weekBefor, $weekAfter])->where('a.status', '1')->sum('extract_price');
        return compact('today', 'yesterday', 'week');
    }


    /**
     * 用户充值统计
     */
    public static function rechargeTotal($member_id)
    {
        $today = $yesterday = $week = 0;
        $member_miniapp_ids = MemberMiniapp::where('member_id', $member_id)->column('id');
        if (!$member_miniapp_ids) return compact('today', 'yesterday', 'week');
        //得到前七天的时间戳
        $weekBefor = date('Y-m-d', strtotime('-7 day'));
        $weekAfter = date('Y-m-d', strtotime('+1 day'));
        //查询昨天的提现数量
        $yesterday = UserRecharge::where('u.member_miniapp_id', 'in', $member_miniapp_ids)->alias('a')->join('user u', 'u.id=a.uid', 'LEFT')->whereDay('a.add_time', 'yesterday')->where('a.paid', 1)->sum('price');
        //今天提现数量
        $today = UserRecharge::where('u.member_miniapp_id', 'in', $member_miniapp_ids)->alias('a')->join('user u', 'u.id=a.uid', 'LEFT')->whereDay('a.add_time', 'today')->where('a.paid', 1)->sum('price');
        //前七天提现数量
        $week = UserRecharge::where('u.member_miniapp_id', 'in', $member_miniapp_ids)->alias('a')->join('user u', 'u.id=a.uid', 'LEFT')->where('a.add_time', 'between time', [$weekBefor, $weekAfter])->where('a.paid', 1)->sum('price');
        return compact('today', 'yesterday', 'week');
    }

    /**
     * 当前应用数量
     */
    public static function appTotal($member_id)
    {
        return MemberMiniapp::where('member_id', $member_id)->count();
    }

    /**
     * 租户数量统计
     */
    public static function addMemberTotal()
    {
        $today = $yesterday = $week = 0;
        //得到前七天的时间戳
        $weekBefor = date('Y-m-d', strtotime('-7 day'));
        $weekAfter = date('Y-m-d', strtotime('+1 day'));
        //查询昨天的用户数量
        $yesterday = self::whereDay('create_time', 'yesterday')->count();
        //今天
        $today = self::whereDay('create_time', 'today')->count();
        //前七天
        $week = self::where('create_time', 'between time', [$weekBefor, $weekAfter])->count();
        $all = self::count();
        return compact('today', 'yesterday', 'week', 'all');
    }


    /**
     * 用户数量统计(总后台)
     */
    public static function allUserTotal()
    {
        $today = $yesterday = $week = 0;
        //得到前七天的时间戳
        $weekBefor = date('Y-m-d', strtotime('-7 day'));
        $weekAfter = date('Y-m-d', strtotime('+1 day'));
        //查询昨天的用户数量
        $yesterday = User::whereDay('create_time', 'yesterday')->count();
        //今天
        $today = User::whereDay('create_time', 'today')->count();
        //前七天
        $week = User::where('create_time', 'between time', [$weekBefor, $weekAfter])->count();
        $all = User::count();//总数
        return compact('today', 'yesterday', 'week', 'all');
    }


    /**
     * 租户充值统计
     */
    public static function memberRechargeTotal()
    {
        $today = $yesterday = $week = 0;
        //得到前七天的时间戳
        $weekBefor = date('Y-m-d', strtotime('-7 day'));
        $weekAfter = date('Y-m-d', strtotime('+1 day'));
        //查询昨天的用户数量
        $yesterday = WalletBill::whereDay('create_time', 'yesterday')->where('type', 'recharge')->sum('value');
        //今天
        $today = WalletBill::whereDay('create_time', 'today')->where('type', 'recharge')->sum('value');
        //前七天
        $week = WalletBill::where('create_time', 'between time', [$weekBefor, $weekAfter])->where('type', 'recharge')->sum('value');
        $all = WalletBill::where('type', 'recharge')->sum('value');
        return compact('today', 'yesterday', 'week', 'all');
    }

    /**
     * 租户购买应用统计
     */
    public static function memberAppTotal()
    {
        $today = $yesterday = $week = 0;
        //得到前七天的时间戳
        $weekBefor = date('Y-m-d', strtotime('-7 day'));
        $weekAfter = date('Y-m-d', strtotime('+1 day'));
        //查询昨天的用户数量
        $yesterday = MemberMiniapp::whereDay('create_time', 'yesterday')->count();
        //今天
        $today = MemberMiniapp::whereDay('create_time', 'today')->count();
        //前七天
        $week = MemberMiniapp::where('create_time', 'between time', [$weekBefor, $weekAfter])->count();
        $all = MemberMiniapp::count();
        return compact('today', 'yesterday', 'week', 'all');
    }


}