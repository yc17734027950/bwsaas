<?php
// +----------------------------------------------------------------------
// | Bwsaas
// +----------------------------------------------------------------------
// | Copyright (c) 2015~2020 http://www.buwangyun.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Gitee ( https://gitee.com/buwangyun/bwsaas )
// +----------------------------------------------------------------------
// | Author: buwangyun <hnlg666@163.com>
// +----------------------------------------------------------------------
// | Date: 2020-9-28 10:55:00
// +----------------------------------------------------------------------

namespace app\common\model;

use buwang\base\BaseModel;
use app\manage\model\AuthNode;
use buwang\exception\MiniappException;
use app\manage\model\AuthGroup;

/**
 * 插件表
 * Class Plugin
 * @package app\common\model
 */
class Plugin extends BaseModel
{

    /**插件角色组
     * @return \think\model\relation\BelongsTo
     */
    public function group()
    {
        return $this->hasMany(AuthGroup::class, 'app_name', 'name');
    }

    /**
     * 得到插件总后台菜单
     */
    public function getAdminMenu($admin_id, $id, $menu_id = 0)
    {
        //得到插件菜单
        $self = self::where('id|name', $id)->find();
        if (!$self) throw new MiniappException('未找到插件');
        //得到系统插件中心节点id
        $node = AuthNode::where('auth_name', 'manage_admin_plugin_core_index')->find();
        if (!$node) throw new MiniappException('当前系统缺少插件中心节点');
        //得到菜单
        $data = $this->getBaseMenu($admin_id, 'admin', $self['name'], 'admin', 'system_plugin', $node['id'], $menu_id);
        //插件路径
        $data['url_info'] = [
            'base'=>'/manage/admin/plugin/core/index',
            'base_name'=>'插件中心',
            'current_name'=>$self['title'],
        ];
        return $data;
    }


    /**
     * 得到插件租户菜单
     */
    public function getMemberMenu($member_id, $id, $menu_id = 0, $app = '')
    {
        //得到插件菜单
        $self = self::where('id|name', $id)->find();
        if (!$self) throw new MiniappException('未找到插件');
        //得到系统插件中心节点id
        $node = AuthNode::where('auth_name', 'manage_member_plugin_core_index')->find();
        if (!$node) throw new MiniappException('当前租户缺少插件中心节点');
        //TODO : 20210508 应用插件中心跳转返回路径错误修复
        $type = "system_plugin";
        $base_url="/manage/member/plugin/core/index";
        if ($app){
            $base_url.="?app={$app}";
            $type = "app_plugin_{$app}";
        }
        //得到菜单
        $data = $this->getBaseMenu($member_id, 'member', $self['name'], 'member', $type, $node['id'], $menu_id);
        //插件路径
        $data['url_info'] = [
            'base'=>$base_url,
            'base_name'=>'插件中心',
            'current_name'=>$self['title'],
        ];
        return $data;
    }

    public function getBaseMenu($user_id, $login_type, $plugin_name, $scope = 'admin', $type = 'system_plugin', $node_id = 0, $menu_id = 0)
    {
        try{
            switch ($login_type) {
                case "admin":
                    //一级菜单
                    $menu = AuthNode::adminAuthWhere($user_id, $type, $scope, $plugin_name, $node_id,[],true)->select()->toArray();
                    //得到有效的是菜单的权限菜单
                    if ($menu_id) {
                        $where = AuthNode::adminAuthWhere($user_id, $type, $scope, $plugin_name, $menu_id,[],true);
                        $list = AuthNode::getMenu($where, '', '', '', $menu_id);
                    } else {
                        if (!$menu) throw new MiniappException('插件无管理菜单');
                        $where = AuthNode::adminAuthWhere($user_id, $type, $scope, $plugin_name, $menu[0]['id'],[],true);
                        $list = AuthNode::getMenu($where, '', '', '', $menu[0]['id']);

                    }
                    break;
                case "member":
                    //一级菜单
                    $menu = AuthNode::memberAuthWhere($user_id, $type, $scope, $plugin_name, $node_id,[],true)->select()->toArray();
                    //得到有效的是菜单的权限菜单
                    if ($menu_id) {
                        $where = AuthNode::memberAuthWhere($user_id, $type, $scope, $plugin_name, $menu_id,[],true);
                        $list = AuthNode::getMenu($where, '', '', '', $menu_id);
                    } else {
                        if (!$menu) throw new MiniappException('插件无管理菜单');
                        $where = AuthNode::memberAuthWhere($user_id, $type, $scope, $plugin_name, $menu[0]['id'],[],true);
                        $list = AuthNode::getMenu($where, '', '', '', $menu[0]['id']);
                    }
                    break;
                default:
                    throw new MiniappException('不支持的菜单类型');
            }
        }catch (\Exception $e){
            throw new MiniappException($e->getMessage());
        }

        return compact('menu', 'list');
    }


}