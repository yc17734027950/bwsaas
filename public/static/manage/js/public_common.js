define(["jquery", "easy-admin"], function ($, ea) {
    var tree = layui.tree;
    var laydate = layui.laydate;
    var laypage = layui.laypage;
    var layer = layui.layer;
    var init = {};
    var Controller = {
        newPic: function () {
            file.init();
            ea.listen();
        },
    };

    var file = {
        data: {
            test: [{
                title: '江西'
                , id: 1
                , children: [{
                    title: '南昌'
                    , id: 1000
                    , children: [{
                        title: '青山湖区'
                        , id: 10001
                    }, {
                        title: '高新区'
                        , id: 10002
                    }]
                }, {
                    title: '九江'
                    , id: 1001
                }, {
                    title: '赣州'
                    , id: 1002
                }]
            }, {
                title: '广西'
                , id: 2
                , children: [{
                    title: '南宁'
                    , id: 2000
                }, {
                    title: '桂林'
                    , id: 2001
                }]
            }, {
                title: '陕西'
                , id: 3
                , children: [{
                    title: '西安'
                    , id: 3000
                }, {
                    title: '延安'
                    , id: 3001
                }]
            }, {
                title: '陕西'
                , id: 3
                , children: [{
                    title: '西安'
                    , id: 3000
                }, {
                    title: '延安'
                    , id: 3001
                }]
            }, {
                title: '陕西'
                , id: 3
                , children: [{
                    title: '西安'
                    , id: 3000
                }, {
                    title: '延安'
                    , id: 3001
                }]
            }, {
                title: '陕西'
                , id: 3
                , children: [{
                    title: '西安'
                    , id: 3000
                }, {
                    title: '延安'
                    , id: 3001
                }]
            }, {
                title: '陕西'
                , id: 3
                , children: [{
                    title: '西安'
                    , id: 3000
                }, {
                    title: '延安'
                    , id: 3001
                }]
            }], //模拟数据

        },
        init: function () {
            this.time_init('#date_time');
            this.cate_init('#cate_list');
            this.page_init('page');
            this.event_init();
            this.delete_init();
            this.select_init();

        },
        time_init: function (el) {
            //日期时间范围
            laydate.render({
                elem: el
                , type: 'datetime'
                , range: true
            });
        },
        cate_init: function (el) {
            //开启节点操作图标
            tree.render({
                elem: el
                , data: file.data.test
                , edit: ['add', 'update', 'del'] //操作节点的图标
                , click: function (obj) {
                    layer.msg(JSON.stringify(obj.data));
                }
            });
        },
        page_init: function (el) {
            //完整功能
            laypage.render({
                elem: el
                , count: 100
                , limit: 8
                , layout: ['count', 'prev', 'page', 'next', 'refresh', 'skip']
                , jump: function (obj) {
                    console.log(obj)
                }
            });
        },
        event_init: function () {
            $(document).ready(function () {
                $(".image-block").mouseover(function () {
                    $(this).find('.image-delete').show();
                });
                $(".image-block").mouseout(function () {
                    $(this).find('.image-delete').hide();
                });
            });
        },
        delete_init: function () {
            $(document).on("click", ".image-delete", function (e) {

                layer.confirm('确认删除文件么？删除后将不可恢复！', {
                    btn: ['确认删除', '取消'] //可以无限个按钮
                }, function (index, layero) {
                    //按钮【按钮一】的回调
                    layer.alert('删除成功', {icon: 1});
                    layer.close(index);
                }, function (index) {
                    //按钮【按钮二】的回调
                    layer.close(index);
                });

            });
        },
        select_init: function () {
            $(".image-block").click(function (e) {
                var attr =   $(this).find('.image-shade').attr('data-check');
                console.log(attr);
                if(attr==1){
                    console.log('隐藏')
                    $(this).find('.image-shade').attr('data-check',0);
                    $(this).find('.image-shade').hide();
                }else{
                    console.log('显示');
                    $(this).find('.image-shade').attr('data-check',1);
                    $(this).find('.image-shade').show();
                }

            });
        }
    }


    return Controller;
});