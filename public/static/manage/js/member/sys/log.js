define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/sys/log/index',
        add_url: 'member/sys/log/add',
        edit_url: 'member/sys/log/edit',
        delete_url: 'member/sys/log/del',
        export_url: 'member/sys/log/export',
        modify_url: 'member/sys/log/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: '管理员操作记录ID'},
                    // {field: 'admin_id', title: '管理员id',minWidth:150},
                    {field: 'member_id', title: '租户id',minWidth:150},
                    // {field: 'user_id', title: '用户id',minWidth:150},
                    {field: 'name', title: '访问者',minWidth:150},
                    {field: 'username', title: '账号',minWidth:150},
                    {field: 'menu_path', title: '访问路由',minWidth:150},
                    // {field: 'path', title: '目标URL',minWidth:150},
                    {field: 'method', title: '访问菜单',minWidth:150},
                    // {field: 'param', title: '参数',minWidth:150},
                    {field: 'ip', title: '登录IP',minWidth:150},
                    {field: 'add_time', title: '操作时间',templet: ea.table.date,minWidth:150},
                    // {field: 'scopes', search: 'select', selectList: {"admin":"总后台","member":"租户后台","mini_program":"小程序","h5":"h5","app":"app","official":"公众号"}, title: '范围',minWidth:150},
                    // {field: 'admin.nickname', title: '昵称'},
                    // {field: 'admin.avatar', title: '头像', templet: ea.table.image},
                    // {field: 'admin.mobile', title: '手机号'},
                    {width: 250, title: '操作', templet: ea.table.tool
                    },
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});