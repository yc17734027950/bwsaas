define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/user/index',
        add_url: 'member/user/add',
        edit_url: 'member/user/edit',
        detail_url: 'member/user/detail',
        edit_pw_url: 'member/user/editPw',
        delete_url: 'member/user/del',
        export_url: 'member/user/export',
        modify_url: 'member/user/modify',
        change_money_url: 'member/user/changeMoney',
        send_url: 'member/message/send',
    };
    var init_detail = {
        table_elem: '#detailTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/user/bill/index',
        add_url: 'member/user/bill/add',
        edit_url: 'member/user/bill/edit',
        delete_url: 'member/user/bill/del',
        export_url: 'member/user/bill/export',
        modify_url: 'member/user/bill/modify',
    };

    function getgetMemberMiniappList() {
        var miniappList = {};
        var select_name = $('#select_name').val();
        console.log(select_name);
        if(select_name){
            miniappList =JSON.parse(select_name);
        }
        return miniappList;
    }

    function getgetMemberMiniappList() {
        var miniappList = {};
        var select_name = $('#select_name').val();
        console.log(select_name);
        if(select_name){
            miniappList =JSON.parse(select_name);
        }
        return miniappList;
    }

    function getTypes() {
        var miniappList = {};
        var select_name = $('#select_type').val();
        console.log(select_name);
        if(select_name){
            miniappList =JSON.parse(select_name);
        }
        return miniappList;
    }

    function getCategorys() {
        var miniappList = {};
        var select_name = $('#select_category').val();
        console.log(select_name);
        if(select_name){
            miniappList =JSON.parse(select_name);
        }
        return miniappList;
    }
    var Controller = {

        index: function () {
            $(document).click(function(event) {
                if ($(event.target).attr('lay-event') != 'more' && $('.more-operation').not(':hidden').length) {
                    $('.more-operation').css('display', 'none');
                }
            });
            var memberMiniappList = getgetMemberMiniappList();//应用搜索列表
            ea.table.render({
                init: init,
                cols: [[
                    // {type: 'checkbox',fixed: 'left'},
                    {field: 'id', title: 'id',searchOp:'=',minWidth:150},
                    {field: 'member_miniapp_id', search: 'select', selectList: getMemberMiniappSelectList, title: '所属应用',minWidth:150 ,templet:function (row) {
                            return getMemberMiniappSelectList[row.member_miniapp_id];
                        },searchOp:'='},
                    // {field: 'password', title: '密码'},
                    {field: 'mobile', title: '手机号'},
                    // {field: 'miniapp_uid', title: 'openid',hide:true},
                    {field: 'nickname', title: '昵称'},
                    {field: 'avatar', title: '头像', templet: ea.table.image,search:false},
                    {field: 'login_ip', title: '登录IP',hide:true},
                    {field: 'login_time', title: '登录时间', search: 'range',templet: ea.table.date,hide:true},
                    {field: 'status', search: 'select', selectList: ["锁定","正常"], tips: '正常|锁定', title: '状态', templet: ea.table.switch},
                    {field: 'integral', title: '用户积分'},
                    {field: 'money', title: '用户余额'},
                    // {field: 'brokerage', title: '佣金',hide:true},
                    {field: 'username', title: '用户名',hide:true},
                    // {field: 'memberMiniapp.appname', search: 'select',fieldAlias:'memberMiniapp.id', selectList: memberMiniappList, title: '应用名称'},
                    {field: 'memberMiniapp.head_img', title: 'Logo', templet: ea.table.image,hide:true,search:false},
                    {field: 'memberMiniapp.miniapp_head_img', title: '小程序logo', templet: ea.table.image,search:false},
                    {field: 'memberMiniapp.mp_head_img', title: '公众号logo', templet: ea.table.image,search:false},
                    {width: 300, title: '操作', fixed: 'right',toolbar: '#listBar'},
                ]],
                tableDataCallback: function (res) {

                }
            });
            //监听工具条
            layui.table.on('tool(currentTableRenderId_LayFilter)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
                var data = obj.data; //获得当前行数据
                var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                switch (layEvent) {
                    case 'edit':
                        ea.open('用户详情', '/manage/'+init.detail_url+'?id='+data.id, '100%', '100%');
                        break;
                    case 'send':
                        ea.open('站内信', '/manage/'+init.send_url+'?id='+data.id, '60%', '80%');
                        break;
                    case 'editPw': //修改密码
                        ea.open('修改密码', '/manage/'+init.edit_pw_url+'?id='+data.id, '60%', '80%');
                        break;
                    case 'more':
                        $('.more-operation').css('display', 'none');
                        $(obj.tr).find('.more-operation').css('display', 'block');
                        break;
                }
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        detail: function (data, row) {
            var memberMiniappList = getgetMemberMiniappList();//应用搜索列表
            var types = getTypes();//类型
            var categorys = getCategorys()//币种
            init_detail.index_url = init_detail.index_url + '?uid=' + detail_id;
            ea.table.render({
                init: init_detail,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: '账单id',width: '6%'},
                    {field: 'uid', title: '用户ID',width: '6%'},
                    {field: 'user.mobile', title: '手机号',hide:true,width: '8%'},
                    {field: 'user.nickname', title: '昵称',hide:true,width: '8%'},
                    {field: 'user.avatar', title: '头像', templet: ea.table.image,width: '5%'},
                    // {field: 'link_id', title: '关联id',minWidth:150},
                    {field: 'pm', search: 'select', selectList: ["支出","获得"], title: '收支', templet: '#pm_templet',width: '4%'},
                    {field: 'title', title: '账单标题',width: '10%'},
                    {field: 'category', search: 'select', selectList: categorys, title: '明细币种',width: '6%'},
                    {field: 'type', title: '明细类型',search: 'select', selectList: types,width: '10%'},
                    {field: 'number', title: '明细数字',templet: '#number_templet',width: '8%'},
                    {field: 'balance', title: '剩余',width: '8%'},
                    {field: 'mark', title: '备注',minWidth:150},
                    {field: 'status', search: 'select', selectList: {"0":"待确认","1":"有效","-1":"无效"}, title: '状态',width: '6%'},
                    {field: 'appname', search: 'select',fieldAlias:'memberMiniapp.id', selectList: memberMiniappList, title: '应用名称',width: '10%'},
                    {field: 'head_img', title: 'Logo',fieldAlias:'memberMiniapp.head_img', templet: ea.table.image,hide:true,minWidth:150},
                    {field: 'miniapp_head_img', title: '小程序logo',fieldAlias:'memberMiniapp.miniapp_head_img', templet: ea.table.image,width: '6%'},
                    {field: 'mp_head_img', title: '公众号logo',fieldAlias:'memberMiniapp.mp_head_img', templet: ea.table.image,width: '6%'},
                    {field: 'add_time', title: '添加时间',minWidth:150,templet: ea.table.date,width: '10%'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });
            ea.listen();
        },
        editPw: function () {
            ea.listen();
        },
        changeMoney: function () {
            ea.listen();
        },
        sendMessage: function () {
            ea.listen();
        },
    };
    var detail_url = function (data) {
        return init.detail_url + '?id=' + data.id
    }
    return Controller;
});