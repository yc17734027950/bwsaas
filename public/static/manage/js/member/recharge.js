define(["jquery", "easy-admin"], function ($, ea, Vue) {

    var form = layui.form;
    var util = layui.util;
    var Controller = {
        index: function () {
            // var app = new Vue({
            //     el: '#app',
            //     data: {
            //         upload_type: 'upload_type'
            //     }
            // });
            //
            // form.on("radio(upload_type)", function (data) {
            //     app.upload_type = this.value;
            // });

            ea.listen(
                function (data) {
                    return data;
                },
                function (res) {
                    var serverTime = new Date().getTime(); //假设为当前服务器时间，这里采用的是本地时间，实际使用一般是取服务端的
                    var endTime = res.data.data.expire_time*1000; //假设为结束日期

                    util.countdown(endTime, serverTime, function(date, serverTime, timer){
                        //var str = date[0] + '天' + date[1] + '时' +  date[2] + '分' + date[3] + '秒';
                        var str = date[2] + '分' + date[3] + '秒';
                        layui.$('#countdown_time').html('支付剩余时间：'+str);
                    });
                    var isQrcode = setInterval(function(){
                        $.getJSON("/manage/recharge/checkOrderStatus?order_sn="+res.data.data.out_trade_no,function(res){
                            if(res.code == 200 && (res.data.data.status == 1)){
                                clearTimeout(isQrcode);
                                ea.msg.success(res.msg, function () {
                                    location.reload();
                                })
                            }else if(res.data.data.status == 2){

                                layui.$('#countdown_time').html('支付超时，请重新下单！');
                                clearTimeout(isQrcode);
                            }
                        })
                    },1500)
                    layer.open({
                        type: 1
                        ,title: '充值余额' //不显示标题栏
                        ,closeBtn: false
                        ,area: '300px;'
                        ,shade: 0.2
                        ,id: 'pay' //设定一个id，防止重复弹出
                        ,btn: ['支付成功', '支付失败']
                        ,btnAlign: 'c'
                        ,moveType: 1 //拖拽模式，0或者1
                        ,content: '<div style="display: flex;justify-content: center;flex-direction: column;align-items: center;">' +
                            '<img style="width: 205px;" src="' + res.data.data.code_url + '">' +
                            '<span id="countdown_time"></span>' +
                            '</div>'
                        ,yes: function(index, layero){
                            layer.close(index);
                            clearInterval(isQrcode);
                            setTimeout(function () {
                                location.reload();
                            }, 1000)
                        }
                    });

                }
            );
        }
    };
    return Controller;
});