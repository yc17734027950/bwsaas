define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/wechat/message/template/index?miniapp_id=' + CONFIG.MINIAPP_ID,
        add_url: 'member/wechat/message/template/add?miniapp_id=' + CONFIG.MINIAPP_ID,
        edit_url: 'member/wechat/message/template/edit',
        delete_url: 'member/wechat/message/template/del',
        export_url: 'member/wechat/message/template/export',
        modify_url: 'member/wechat/message/template/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'ID'},
                    {field: 'name', title: '模板名'},
                    {field: 'key', title: '模板标识'},
                    {field: 'template_id', title: '模板ID'},
                    {field: 'status', search: 'select', selectList: ["关闭", "开启"], tips: '开启|关闭', title: '状态', templet: ea.table.switch},
                    {field: 'create_time', title: '创建时间', templet: ea.table.date},
                    {width: 250, title: '操作', templet: ea.table.tool},

                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});