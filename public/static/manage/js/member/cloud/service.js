define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member/cloud/service/index',
        detail_url: 'member/cloud/service/detail',
    };
    var laytpl = layui.laytpl;
    var dropdown = layui.dropdown
        , util = layui.util;
    var element=layui.element;

    var Controller = {
        index: function () {
            service.init();
            element.init();
            ea.listen();
        },
        detail: function () {
            element.init();
            service.data.check = false
            service.choose_listen();
            ea.listen();
            service.detail_init();
            service.log_listen();
            service.select_packet_listen();
            service.buy_listen();
        }
    };

    //操作对象
    var service = {
        data: {
            service_id: 0,
            service_name:'',
            service_info:'',
            buy:true,
            list:[],
            url:'',
            check: false,
            packet_id:'',
            packet_price: '',
        },
        init: function () {
            //加载列表
            this.setServiceList();
            this.detail_listen();
        },
        setServiceList: function () {
            var _this = this;
            ea.request.post({
                url: '/' + CONFIG.ADMIN + '/' + init.index_url,
            }, function (res) {
                _this.data.list = res.data.data.list;
                _this.loadList(res.data.data, laytpl);//插件列表渲染
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('查询失败');//异常
            });
        },
        loadList: function (data, laytpl) {
            //渲染插件
            var getTpl = service_template.innerHTML
                , view = document.getElementById('service_list');
            laytpl(getTpl).render(data.list, function (html) {
                view.innerHTML = html;
            });
        },
        detail_listen:function () { //插件详情
            var _this = this;
            $('body').on("click",".detail",function(event){
                _this.data.service_id = $(this).attr('data-service-id');
                _this.data.service_name = $(this).attr('data-service-name');
                window.location.href = '/manage/member/cloud/service/detail?service_id='+_this.data.service_id+'&service_name='+_this.data.service_name;

            });
        },
        choose_listen: function() {
            $("body").on('click', '.details-combo-items', function (event) {
                    $(".details-combo-items").eq($(this).index()).css("border", "1px solid #B3B3B3").siblings().css("border","1px solid #f2f2f2")
            });
        },
        log_listen: function() {

            $('body').on('click', '[lay-event]', function () {
                var layEvent = $(this).attr('lay-event');
                switch (layEvent) {
                    case 'packet':
                        ea.open('【' + service_name + '】套餐',"/manage/member/cloud/packet/index?service_id=" + service_id,'50%','90%');
                        break;
                    case 'walletBill':
                        ea.open('【' + service_name + '】订单',"/manage/member/cloud/walletBill/index?service_id=" + service_id,'50%','90%');
                        break;
                    case 'order':
                        ea.open('【' + service_name + '】记录',"/manage/member/cloud/order/index?service_id=" + service_id,'50%','90%');
                        break;

                }
            });


        },
        select_packet_listen: function() {
            var _this = this;
            $('body').on('click', '[lay-selected-id]', function () {
                _this.data.packet_id = $(this).attr('lay-selected-id');
                _this.data.packet_price = $(this).attr('lay-selected-price');
                $("#select_price").html(_this.data.packet_price);
            });
        },
        detail_init: function (callback,check) {
            var _this = this;
            ea.request.post({
                url: '/' + CONFIG.ADMIN + '/' + init.detail_url+'?service_id='+service_id+'&service_name='+service_name
            }, function (res) {
                if(callback)callback(res.data.data);
                if(!check) {
                    _this.loadServiceDetail(res.data.data.service, laytpl);
                    _this.loadPacketList(res.data.data.packet, laytpl);
                }
            }, function (res) {
                ea.msg.error(res.msg); //失败
            }, function (that) {
                ea.msg.error('查询失败');//异常
            });
        },
        loadServiceDetail: function (data, laytpl) {
            //渲染插件
            var getTpl = service1_template.innerHTML
                , view = document.getElementById('service1');
            console.log(data);
            laytpl(getTpl).render(data, function (html) {
                view.innerHTML = html;
            });
        },
        loadPacketList: function (data, laytpl) {
            //渲染插件
            var getTpl = packet_template.innerHTML
                , view = document.getElementById('packet');
            laytpl(getTpl).render(data, function (html) {
                view.innerHTML = html;
            });
        },
        buy_listen() {
            var _this = this;
            $('body').on('click', '#buy', function () {
              if(!_this.data.packet_id) {
                  layer.msg('请选择正确的套餐', {icon: 2, time:1000});
                  return
              }
            //TODO 弹出一个弹窗获取支付密码
                layer.open({
                    title: '确认购买',
                    type: 1,
                    area: ['300px', '180px'],
                    closeBtn: 0,
                    content: "<input id='paypwd' type=\"number\" placeholder=\"请输入6位数字支付密码\" class=\"layui-input\">"
                    ,shadeClose: true
                    ,btn: ['支付']
                    , btnAlign: 'c' //按钮居中
                    ,yes: function(index, layero){
                        var paypwd = $('#paypwd').val();
                        if(!paypwd) {
                            layer.msg('请填写支付密码', {icon: 2, time:1000});
                            return;
                        }

                        var data = {
                            id: _this.data.packet_id,
                            paypwd: paypwd,
                        };

                        ea.request.post({
                            url: '/manage/member/cloud/packet/buy',
                            data: data,
                        }, function (res) {
                            ea.msg.success('购买成功');
                            window.location.reload();
                        }, function (res) {
                            ea.msg.error(res.msg); //失败
                        }, function (that) {
                            ea.msg.error('失败');//异常
                        });
                    }
                });
                return;
            })
        }

    };

    return Controller;
});