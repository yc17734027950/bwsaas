define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'admin/sys/notice/index',
        add_url: 'admin/sys/notice/add',
        edit_url: 'admin/sys/notice/edit',
        delete_url: 'admin/sys/notice/del',
        export_url: 'admin/sys/notice/export',
        modify_url: 'admin/sys/notice/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', width:80, title: 'id'},
                    {field: 'cate_id', align:'left', title: '分类id'},
                    {field: 'noticeCategory.name', minWidth:100, align:'left', title: '分类名称'},
                    {field: 'title', minWidth:200, align:'left', title: '标题'},
                    {field: 'status', width:100, title: '状态', templet: ea.table.switch},
                    {field: 'add_time', width:160, title: '发布时间',templet: ea.table.date},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});