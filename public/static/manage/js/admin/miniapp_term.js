define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'admin/miniapp/term/index',
        add_url: 'admin/miniapp/term/add',
        edit_url: 'admin/miniapp/term/edit',
        delete_url: 'admin/miniapp/term/del',
        export_url: 'admin/miniapp/term/export',
        modify_url: 'admin/miniapp/term/modify',
    };

    var Controller = {
        index: function () {
            init.index_url += '?miniapp_id='+miniapp_id;
            init.add_url += '?miniapp_id='+miniapp_id;
            init.edit_url += '?miniapp_id='+miniapp_id;
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'price', title: '套餐售价'},
                    {field: 'days', title: '套餐天数',templet:function (data) {
                            if(data.days ==0)return '永久';
                            return data.days + '天';
                        }},
                    {field: 'name', title: '套餐名称'},
                    {field: 'miniapp_id', title: '应用id'},
                    {field: 'miniapp.dir', title: '目录'},
                    {field: 'miniapp.title', title: '名称'},
                    {field: 'status', width:100, title: '状态', templet: ea.table.switch},
                    {field: 'miniapp_module_id', title: '基础模块id',hide:true},
                    {field: 'desc', title: '描述',hide:true},
                    {field: 'weight', title: '排序'},
                    {field: 'create_time', title: '创建时间',templet: ea.table.date},

                    {width: 250, title: '操作', templet: ea.table.tool},

                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});