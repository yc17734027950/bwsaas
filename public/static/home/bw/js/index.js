var mySwiper = new Swiper ('.swiper-container',{
			  direction: 'horizontal',// 垂直切换选项
			  loop: true,// 循环模式选项
			  autoplay: true,//可选选项，自动滑动
			  pagination: {
			    el: '.swiper-pagination',
				type:'custom',
				renderCustom:function(swiper,current,total){
					let paginations = document.getElementsByClassName("pagination");
					for(var i=0;i<total;i++){
						if(i == (current-1)){
							paginations[i].className = "pagination bg_url1";
						}else{
							paginations[i].className = "pagination bg_url2";
						}
					}
				}
			  },
});
var paginations = document.getElementsByClassName('pagination');
for(var i=0;i<paginations.length;i++){
	paginations[i].onclick=function(e){
		var num = e.target.getAttribute('data-num');
		mySwiper.slideTo(Number(num)+1,1000,false);
		mySwiper.autoplay.start();
	}
}
