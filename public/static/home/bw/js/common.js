$(function(){
	// 头部
	$(window).on('scroll',function(){
		if($(document).scrollTop()>10){
			$('.header').addClass('solid');
			$('.header .signin').addClass('bgc');
			$('.header .signup').addClass('bgcolor');
			$('.header .menu li a').addClass('colors');
		}else{
			$('.header').removeClass('solid');
			$('.solid').removeClass('solid');
			$('.header .signin').removeClass('bgc');
			$('.header .signup').removeClass('bgcolor');
			$('.colors').removeClass('colors')
		}
	});
	//侧边客服
	$(".toolitembar_flex>a").eq(0).hover(function(){
			$(".call").css("display","block")
			$(this).css("background-color","#21d376")
		},function(){
			$(".call").css("display","none")
			$(this).css("background-color","rgba(3, 169, 244, .7)")
		}
	);
	$(".toolitembar_flex>a").eq(1).hover(function(){
			$(".telephon").css("display","block")
			$(this).css("background-color","#ffa900")
		},function(){
			$(".telephon").css("display","none")
			$(this).css("background-color","rgba(3, 169, 244, .7)")
		}
	);
	$(".toolitembar_flex>a").eq(2).hover(function(){
			$(".yanshi").css("display","block")
			$(this).css("background-color","#00c8f2")
		},function(){
			$(".yanshi").css("display","none")
			$(this).css("background-color","rgba(3, 169, 244, .7)")
		}
	);
	$(".toolitembar_flex>a").eq(3).hover(function(){
			$(".zhuce").css("display","block")
			$(this).css("background-color","#FF697B")
		},function(){
			$(".zhuce").css("display","none")
			$(this).css("background-color","rgba(3, 169, 244, .7)")
		}
	);
});