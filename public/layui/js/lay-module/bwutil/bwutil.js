function getsec(str){
    var str1=str.substring(1,str.length)*1;
    var str2=str.substring(0,1);
    if (str2=="s"){
        return str1*1000;
    }else if (str2=="h"){
        return str1*60*60*1000;
    }else if (str2=="d"){
        return str1*24*60*60*1000;
    }
}

layui.define("layer",function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
    // toplayer : window.top.layer ,   // 获取顶层窗口的layer对象
    //     topWin : window.top ,           // 获取顶层窗口对象
    var layer = layui.layer;
    //这是有设定过期时间的使用示例：
    //s20是代表20秒
    //h是指小时，如12小时则是：h12
    //d是天数，30天则：d30
    //setCookie("name","hayden","s20");
    var util = {
        setCookie:function (key, value, times) {
            // var date = new Date();
            // date.setTime(date.getTime() + times);
            // document.cookie = key + '=' + value + ';expires=' + date.toGMTString() + ';path=/';
            var strsec = getsec(times);
            var exp = new Date();
            exp.setTime(exp.getTime() + strsec*1);
            document.cookie = key + "="+ escape (value) + ";expires=" + exp.toGMTString() + ';path=/';
        },
        getCookie:function (key) {
            // var cookies = document.cookie.split('; ');//注意，这块分割的是"; ",分号空格
            // for (var i = 0; i <= cookies.length; i++) {
            //     //console.info(cookies[i])
            //     if (cookies[i]){
            //         var itemCookies = cookies[i].split('=')
            //     }
            //     if (itemCookies[0] === key) {
            //         return itemCookies[1]
            //     }
            // }
            var arr,reg=new RegExp("(^| )"+key+"=([^;]*)(;|$)");
            if(arr=document.cookie.match(reg)) return unescape(arr[2]);
            else return null
        },
        delCookie:function (key) {
            //this.setCookie(key, '', -1);
            var exp = new Date();
            exp.setTime(exp.getTime() - 1);
            var cval=this.getCookie(key);
            if(cval!=null) document.cookie= key + "="+cval+";expires="+exp.toGMTString() + ';path=/';
        },
        // 错误弹出层
        error: function(message) {
            layer.open({
                content:message,
                icon:2,
                title : '错误提示',
            });
        },

        //成功弹出层
        success : function(message,url) {
            layer.open({
                content : message,
                icon : 1,
                yes : function(){
                    location.href=url;
                },
            });
        },
        tips: function (content, target, time) {
            return layer.tips(content, target, { time: time || 3000 });
        },
        showMsg: function (content, url, time){
            layer.msg(content, {time: time || 3000}, function () {
                window.location = url;
            });
        },
        //关闭一个层
        close: function (index) {
            layer.close(index);
        },
        //显示遮罩层
        showLoading: function (time, style) {
            time = time || 0;
            style = style || 1
            return layer.load(style, { shade: [0.5, '#393D49'], time: time });
        },
        //关闭遮罩层
        closeLoading: function () {
            layer.closeAll('loading');
        },
        //弹出提示 icon类型 0:带！的提示 1:√ 2:x 3:? 4:锁 5:难过脸 6:微笑脸
        showTips: function (content, icon, time, callback) {
            icon = icon || 0;
            time = time || 3000;
            layer.msg(content, {
                icon: icon,
                time: time,
                resize: false,
                shade: 0.5,
                shadeClose: true
            }, function () {
                //do something
                if (callback) {
                    callback();
                }
            });
        },
        //与 JS alert相似
        alert: function (content, btnText, title) {
            layer.alert(content, { closeBtn: 1, btn: [btnText], title: title, resize: false })
        },
        //与JS confirm相似
        confirm: function (content, title, sureText, sureFunction, closeText, closeFunction) {
            title = title || "";

            layer.confirm(
                content, { title: title, icon: 3, btn: [sureText, closeText], resize: false },
                function (index) {//确定回调
                    if (sureFunction) {
                        sureFunction();
                    }
                    layer.close(index);
                }, function (index) {//cancel回调
                    if (closeFunction) {
                        closeFunction();
                    }
                    layer.close(index);
                });
        },
        openWin: function (content, title, width, height) {
            title = title || "";
            width = width || 500;
            height = height || 500;

            var offset = ((window.screen.height - height) * 0.3) + 'px';

            return layer.open({
                title: title
                , content: content
                , type: 1
                , offset: offset //具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
                , shade: 0.5 // 遮罩层透明度
                , resize: false
                , area: [width + 'px', height + 'px']
                , skin: 'layui-layer-rim'
            });
        },
        openFrame: function (url, title, width, height) {
            title = title || "";
            width = width || 500;
            height = height || 500;
            var offset = ((window.screen.height - height) * 0.3) + 'px';

            return layer.open({
                title: title
                , content: url
                , type: 2
                , offset: offset //具体配置参考：http://www.layui.com/doc/modules/layer.html#offset
                , shade: 0.5 // 遮罩层透明度
                , resize: false
                , area: [width + 'px', height + 'px']
                , skin: 'layui-layer-rim'
            });
        }

    };

    //输出接口
    exports('bwutil', util);
});



