layui.define(["bwajax" ,"layer" ,"jquery"],function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);

    var bwajax = layui.bwajax.instance(),
        $ = layui.$,
        layer = layui.layer;

    var manage = {
        testR:function () {

            bwajax.post('manage/admin/login')
                .then(function (response) {
                    console.log('response33333');
                });
        },
        adminLogin:function (params) {
            return bwajax.post('manage/admin/login',params);
        },
        logout:function (params) {
            return bwajax.post('manage/admin/logout',params);
        },
        logoutMember:function (params) {
            return bwajax.post('manage/member/logout',params);
        },
        captchasrc:function (params) {
            return bwajax.get('manage/bwCaptcha',params);
        },
        memberLogin:function (params) {
            return bwajax.post('manage/member/login',params);
        },
        getCategory:function (params) {
            return bwajax.post('bwmall/admin.BwmallShopProduct/getCategory',params);
        },
        getNodeTree:function (params) {
            return bwajax.post('manage/node/getNodeTree',params);
        }

    };

    //输出test接口
    exports('manage', manage);
});