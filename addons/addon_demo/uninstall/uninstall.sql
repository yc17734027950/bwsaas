SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ai_goodpay_advert
-- ----------------------------
DROP TABLE IF EXISTS `bw_goodpay_advert`;

-- ----------------------------
-- Table structure for ai_goodpay_bill
-- ----------------------------
DROP TABLE IF EXISTS `bw_goodpay_bill`;

-- ----------------------------
-- Table structure for ai_goodpay_store
-- ----------------------------
DROP TABLE IF EXISTS `bw_goodpay_store`;

SET FOREIGN_KEY_CHECKS=1;
