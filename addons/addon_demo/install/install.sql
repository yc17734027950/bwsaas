SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bw_goodpay_advert
-- ----------------------------
DROP TABLE IF EXISTS `bw_goodpay_advert`;
CREATE TABLE `bw_goodpay_advert` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT '0',
  `member_id` int(11) DEFAULT '0',
  `url` text,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bw_goodpay_bill
-- ----------------------------
DROP TABLE IF EXISTS `bw_goodpay_bill`;
CREATE TABLE `bw_goodpay_bill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `mchid` int(11) DEFAULT NULL,
  `total_fee` decimal(10,2) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `out_trade_no` varchar(255) DEFAULT NULL,
  `notify_url` varchar(255) DEFAULT NULL,
  `is_success` tinyint(1) DEFAULT '0',
  `transaction_id` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `is_notify` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ORDERID` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bw_goodpay_store
-- ----------------------------
DROP TABLE IF EXISTS `bw_goodpay_store`;
CREATE TABLE `bw_goodpay_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `mchid` bigint(20) DEFAULT NULL,
  `store_name` varchar(255) DEFAULT NULL,
  `applyment_id` varchar(100) DEFAULT NULL,
  `merchant_shortname` varchar(100) DEFAULT NULL,
  `store_entrance_pic` varchar(255) DEFAULT NULL,
  `store_entrance_pic_media_id` varchar(255) DEFAULT NULL,
  `indoor_pic` varchar(255) DEFAULT NULL,
  `indoor_pic_media_id` varchar(255) DEFAULT NULL,
  `store_street` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(20) DEFAULT NULL,
  `product_desc` varchar(255) DEFAULT NULL,
  `id_card_valid_time` varchar(255) DEFAULT NULL,
  `id_card_name` varchar(30) DEFAULT NULL,
  `id_card_number` varchar(30) DEFAULT NULL,
  `id_card_copy` varchar(255) DEFAULT NULL,
  `id_card_copy_media_id` varchar(255) DEFAULT NULL,
  `id_card_national` varchar(255) DEFAULT NULL,
  `id_card_national_media_id` varchar(255) DEFAULT NULL,
  `id_card_start_time` varchar(100) DEFAULT NULL,
  `id_card_end_time` varchar(100) DEFAULT NULL,
  `account_bank` varchar(100) DEFAULT NULL,
  `account_number` varchar(50) DEFAULT NULL,
  `bank_address_code` varchar(30) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `business_code` varchar(50) DEFAULT NULL,
  `is_types` tinyint(1) DEFAULT '0',
  `is_default` tinyint(1) DEFAULT '0',
  `is_pass` tinyint(1) DEFAULT '0',
  `is_delete` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0',
  `secret` varchar(255) DEFAULT NULL,
  `service_type` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
