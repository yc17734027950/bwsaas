<?php

return array (
  'tips' => 
  array (
    'name' => 'tips',
    'title' => '温馨提示:',
    'type' => 'string',
    'value' => '该提示将出现的插件配置头部，通常用于提示和说明',
  ),
  'key2' => 
  array (
    'name' => 'key2',
    'title' => '应用key2',
    'type' => 'number',
    'content' => 
    array (
    ),
    'value' => '8',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'key3' => 
  array (
    'name' => 'key3',
    'title' => 'key3',
    'type' => 'datetime',
    'content' => 
    array (
    ),
    'value' => '2020-11-05 00:00:00',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'key4' => 
  array (
    'name' => 'key4',
    'title' => 'key4',
    'type' => 'select',
    'content' => 
    array (
      1 => '显示',
      0 => '不显示',
    ),
    'value' => '1',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'key5' => 
  array (
    'name' => 'key5',
    'title' => 'key5',
    'type' => 'checkbox',
    'content' => 
    array (
      1 => '显示',
      0 => '不显示',
    ),
    'value' => '1,0',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'key' => 
  array (
    'name' => 'key',
    'title' => '应用key',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'xxxxxxxxxxxxxxxx',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'secret' => 
  array (
    'name' => 'secret',
    'title' => '密钥secret',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'xxxxxxxxxxxxxxxx',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'sign' => 
  array (
    'name' => 'sign',
    'title' => '签名',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'xxxxxxxxxxxxxxxx',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
  'template' => 
  array (
    'name' => 'template',
    'title' => '短信模板',
    'type' => 'string',
    'content' => 
    array (
    ),
    'value' => 'xxxxxxxxxxxxxxxx',
    'verify' => 'required',
    'msg' => '',
    'tip' => '',
    'ok' => '',
    'extend' => '',
  ),
);
